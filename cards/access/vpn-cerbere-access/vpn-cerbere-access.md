---
mount: access/vpn-cerbere-access
name: "Connect to Cerbere via VPN"
tags:
  - access
redirects:
  - access:vpn-cerbere-access
  - /external/access/vpn-cerbere-access/
  - /cards/access:vpn-cerbere-access
  - /external/cards/access:vpn-cerbere-access
  - /external/external/access/vpn-cerbere-access/
---


# Connect to Cerbere via VPN

1. [Launch the VPN client](/access/vpn-access) and enter

   ![vpn-connect-01.png](img/vpn-connect-01.png)

2. Enter

   * Username - firstname.lastname
   * Password - VPN password that you have set.

3. Once you are connected to VPN, download RDP files from [Cerbere](https://cerbere.uni.lu/)

   * Enter same credentials as VPN
     * Username - firstname.lastname
     * Password - VPN password that you have set.

   * Under **My Authorizations**, select **Sessions** and download the file

## Access to the VM

Run the command/script you have downloaded from Cerbere. For example like below -

```ssh -t firstname.lastname@cerbere.uni.lu 'Interactive@als-epilepsy-vm-1:SSH:ALS-epilepsy-Group'```

```
WARNING: Access to this system is restricted to duly authorized users only. Any attempt to access this system without authorization or fraudulently remaining within such system will be prosecuted in accordance with the law.
Any authorized user is hereby informed and acknowledges that his/her actions may be recorded, retained and audited.

user's password: VPN password

Account successfully checked out

Connecting to Interactive@als-epilepsy-vm-1:SSH...
Target login: firstname.lastname
firstname.lastname's password: LUMS Password

You are hereby informed and acknowledge that your actions may be recorded, retained and audited in accordance with your organization security policy.
Please contact your WALLIX Bastion administrator for further information.

Last login: Fri Dec  6 16:43:22 2019 from cerbere.uni.lu
```
