---
mount: access/wifi-access-guests
name: "WiFi network access for guests"
tags:
  - access
redirects:
  - access/wifiNetworkAccessGuests
  - access:wifiNetworkAccessGuests
  - /external/access/wifiNetworkAccessGuests/
  - /cards/access:wifiNetworkAccessGuests
  - /external/cards/access:wifiNetworkAccessGuests
  - /external/external/access/wifiNetworkAccessGuests/
---

# WiFi network access for guests

- Each UL staff member can provide wireless network access for their visitors for 5 days.
- Such a visitor account has a lifetime of maximum 5 days. If you need accounts for people staying longer, please open a [ticket via the SIU helpdesk](https://service.uni.lu/sp).
- Guests will only have access to the internet and public services but not to internal resources.

## How-to connect to free public WiFi?

1. Connect to the WiFi network named `Cité des Sciences` from your device
1. Click on `Or register for guest access` **or** enter your credentials if you are already registered
1. Fill in the form (name, e-mail address and phone number)
1. Read and accept the user policy by clicking on `I agree to the terms and conditions` button
1. Click on the `Register` button. Your credentials will be delivered to you via SMS or e-mail
1. Enter your credentials and click on `Sign On`

If you need a WiFi network for an event with large number of guests, please [create a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=79423e65dbd2d8902fa838aa7c961900&sysparm_category=4d5c0a05db9f10502fa838aa7c961925).

For any questions please open a [ticket via the SIU helpdesk](https://service.uni.lu/sp).

[comment]: Process owner: Julia Kessler
