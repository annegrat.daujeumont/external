---
mount: general/links
name: "Important websites"
order: 600
tags:
  - general
redirects:
  - general:links
  - /external/general/links/
  - /cards/general:links
  - /external/cards/general:links
  - /external/external/general/links/
---

# Important websites

This is an overview on websites to manage work and help you with the navigation on work-related matters at the University of Luxembourg.

| Link | Description |
| :------ | :------ |
|[www.uni.lu](https://www.uni.lu)|General website of the University. You can find general information as well as specific information on the faculties and institutes here.|
|[uniluxembourg.sharepoint.com](https://uniluxembourg.sharepoint.com/) / [intranet.lcsb.uni.lu](https://uniluxembourg.sharepoint.com/sites/lcsb/lcsb_internal/SitePages/default.aspx)|The internal platform of the University / the LCSB **Please note:** When outside the university network, you'll need to log in with your university credentials.
|[service.uni.lu](https://service.uni.lu/sp)|Here you find service and support from the university. You can create tickets on all regulatory and legal issues as well as on IT-related topics, e.g. to get access to specific folders and software.|
|[lums.uni.lu](https://lums.uni.lu)|The LUMS (LCSB User Management System) allows you to get access to internal resources such as software provided and/or hosted by the LCSB. **Please note:** You need to request an additional account to be able to log in as your usual UNI.LU-credentials will **not do the job.** The LUMS-accounts can also be created for external researchers and collaborators.|
|[howto.lcsb.uni.lu](https://howto.lcsb.uni.lu/)|This is where the How-to Cards are deposited and constantly updated. You can find answers on administrative as well as on lab-related procedures here. For complex instructions, a step-by-step tutorial with corresponding screenshots or examples is provided. We encourage you to bookmark this page as many answers to your questions might be covered by a card. If you have any suggestions for improvements on existing or additional How-to Cards, just contact the R3 department.|
|[owncloud.lcsb.uni.lu](https://owncloud.lcsb.uni.lu/)|LCSB ownCloud is a private cloud storage service for the use of LCSB staff and collaborators. It is suitable for exchanging small-sized files (up to 1-2 gigabyte, all SSL encrypted). You need a [LUMS account](/access/lums-passwords) for using this platform. The LUMS accounts can also be provided to external collaboration partners and can be requested at the [Service Portal](https://service.uni.lu/sp). For more information see also the [User’s Guide for ownCloud](/exchange-channels/owncloud).|
|[daisy.lcsb.uni.lu](https://daisy.lcsb.uni.lu/)|The DAta Information SYstem (DAISY) is a comprehensive tool to manage your research data in a GDPR-compliant manner. You can create projects & datasets and share them with your collaboration partners. For more information and detailed description on DAISY, please refer to the [DAISY documentation](https://elixir.pages.uni.lu/daisy-doc/).|
|[gitlab.lcsb.uni.lu](https://gitlab.lcsb.uni.lu/)|A developer’s platform operated by the R3-team where you can contribute to publish and develop public protocols/procedures, etc.  For instance, these How-to-cards can be created there. So also the corresponding card on [how to contribute](/contribute/web-ide).|
|[https://outlook.office.com/mail/](https://outlook.office.com/mail/)|The Outlook Web Application to access your mail account via any web browser. Login with you UNI.LU-credentials.|
|[fiori.uni.lu](https://fiori.uni.lu/fiori/)|Upon login with your UNI.LU-credentials, this site provides you information on your employee profile. You can submit leave requests via this portal and also view your payslips.|
|[archibus.uni.lux](https://archibus.uni.lux)|Booking a room in BT2 ground floor, Room 212 in BT1 and meeting rooms in other university buildings. **Please note:** This site can only be accessed when you are logged into the university network, either in your office or via a VPN client from elsewhere. See also the [corresponding How-to Card](/access/vpn-access). Login with your staff/student credentials.|
|[affluences.com](https://affluences.com/universite-du-luxembourg-1/luxembourg-learning-centre/reservation)|Booking a room in the **Luxembourg Learning Centre** at the University Campus in Belval is done through affluences.com - Login with your staff/student credentials.|
