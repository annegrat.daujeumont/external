---
mount: general/remote-working
name: "Work remotely"
order: 800
tags:
  - general
redirects:
  - general:remote-working
  - /external/general/remote-working/
  - /cards/general:remote-working
  - /external/cards/general:remote-working
  - /external/external/general/remote-working/
---

# Work remotely

In case you need to work remotely, there are several tools and services that you can use to stay connected and continue your work.

An important reference is the [list of important links](/general/links). (**Note:** If the links do not work, please refer to the section **Connecting to the university network** since you are most probably not connected to the university internal network.)

There is also a set of [tutorials](https://remote.uni.lu/) prepared by the university IT department you can refer to.

## Connecting to the university network

If you want to access the university infrastructure, such as the servers or storage, you can do so by connecting
via [Virtual Private Network (VPN)](http://vpn.uni.lu). All details are given [here](/access/vpn-cerbere-access).

## Instant messaging

Every university empoyee has access to the full Microsoft 365 suit, which incluses Mircosoft Teams. You can download the latest version of Teams [here](https://www.microsoft.com/en-us/microsoft-teams/download-app#download-for-desktop1). Log in using your @uni.lu credentials.
Every staff member is automatically added to the T_LCSB channel. If your group had a dedicated channel or group chat, contact your team assistant or PI to add you to the relevant channels and chats.

## Video conferencing and virtual meetings

If you want to hold online (virtual) meetings, you can do so by logging in to
[Cisco Webex Meetings](https://unilu.webex.com) using your university credentials.
From there, you can schedule your own meetings and/or join existing sessions.

If you need help setting up and using Cisco Webex, you can follow [these instructions](https://using-webex.uni.lu/).

If you want to take notes, you can do so by drawing on the shared screen. Once done, you can `Save to PDF`.

## Webcam alternatives

In case you want to do video conferences, but do not have a webcam, you can use your phone instead. There are several apps available that allow your phone to act as a webcam for your computer.

For Android phones you can use [DroidCam Wireless Webcam](http://www.dev47apps.com/). It comes with a desktop client for Windows and Linux, but also works on MacOS without the client.

On Apple iPhones (and Android as well) you can use [EpocCam](https://www.elgato.com/en/epoccam). This requires you to install drivers on your computer as well.

Both apps support connecting to your computer either via WiFi or USB. EpocCam supports USB connection only for iOS, though. With DroidCam, you need to [install ADB, enable developer options and USB debugging](http://www.dev47apps.com/droidcam/connect/), if you want to connect your phone via USB.

Apple has recently implemented a native way of sharing an iPhone camera with a Mac running MacOS: Continuity Camera.
It works automatically as long as both devices are up-to-date and using the same apple ID. The phone should appear as a camera option in Teams or Webex.
More information [here](https://support.apple.com/en-us/102546).

## Virtual whiteboards

If you want to take notes collaboratively, you can use the following services:

- [Microsoft Whiteboard](https://whiteboard.microsoft.com/): Login using your UL credentials
- [Aggie](https://aggie.io/)

## Collaborative and real-time note taking / writing

You can use the following tools to take notes or writing manuscripts together:

- [Etherpad](https://hpc.uni.lu/pad/): for any text
- [Overleaf](https://www.overleaf.com): primarily for LaTeX manuscripts

## Sharing files

When on-the-go or working remotely, the [LCSB ownCloud](https://owncloud.lcsb.uni.lu) is the best way to share and work collaboratively on files.

More details on how to set up ownCloud are provided [here](/exchange-channels/owncloud). You can share your files and folders
directly with your collaborators by following [these instructions](/exchange-channels/owncloud/share-files).

## Development of code

Use the [LCSB R3 Gitlab](https://gitlab.lcsb.uni.lu) to develop and share code.

## Forwarding phone calls

If you want to forward phone calls from your office phone to another number, follow these steps:

1. Connect to the university VPN (see instructions above).
2. Go to the phone system management interface on [iptel.uni.lux](http://iptel.uni.lux/).
3. Login with your university credentials.
4. Click on the "FORWARDS" button on the right.
5. Check the box in front of "Always".
6. Enter the number to forward to in the text field after "Always". Please make sure to enter the number in the usual format: "0" for exit and then if needed "00" if it's a foreign number, e.g.:
    * 00049... for German phone numbers
    * 00033... for French phone numbers
    * 0621...  for Luxembourgish mobile numbers
7. Click on "Save" at the bottom right of the page.

You can find more options for call forwarding in [the voice mail options in OWA](https://owa.uni.lu/owa/?bO=1#path=/options/callanswering). There you can set up call forwarding during specific times, when your calendar shows you as busy, for specific callers only or give a selection of several numbers to forward to.

The drawback of setting it up in OWA is that it will still always first ring your office phone and only when you do not pick up the settings will take effect. Also, **forwarding to foreign numbers is not possible in OWA**.
