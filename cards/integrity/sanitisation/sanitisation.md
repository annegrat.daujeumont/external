---
mount: integrity/sanitisation
name: "Sanitising Data Files"
tags:
  - integrity
  - data
redirects:
  - integrity:sanitisation
  - /external/integrity/sanitisation/
  - /cards/integrity:sanitisation
  - /external/cards/integrity:sanitisation
  - /external/external/integrity/sanitisation/
---


# Sanitising Data Files

This How-to Card lists software that can be used to permanently delete **individual files** from storage. If you need to sanitise an entire storage device (e.g. laptop, external hard drive), this will be done for you by University IT Services, please create a ticket on [ServiceNow](https://service.uni.lu/) (using item **Catalogue/IT/Information Security Services** ).

The following table lists software that can be used on different platforms for permanent file removal.

|Platform|Instructions|
|--------|---------------------|
| macOS |For OSX versions prior to 10.11 (El Capitan) you may use the **Secure Empty Trash**  option, which appears when right clicking on the Trash icon.|
| |OSX version 10.11 (El Capitan) onwards, Apple has deprecated the  **Secure Empty Trash**, as it is not effective on Solid State Drives (SSDs). The approach to be adopted on Macs is to [turn on disk encryption](/external/integrity/encryption/disk) for your entire disk, or to keep sensitive data files in [encrypted disk images](/external/integrity/encryption/file).|
| Linux | Use the **shred** command, instructions can be found [here](https://www.lifewire.com/shred-linux-command-4094148).|
| Windows | Use the **SDelete** command line utility, instructions can be found [here](https://docs.microsoft.com/en-us/sysinternals/downloads/sdelete).|

Please note if your files reside on server-side storage then permanent file removal may not take full effect. This is due to (1) the storage redundancy features on some server platforms, or (2) automatic server backups. To get more information, and to request removal of backups, you may contact University or LCSB IT Support by creating a ticket on [ServiceNow](https://service.uni.lu/).
