---
mount: contribute
name: "Contributing to How-to Cards: Overview"
tags:
  - contribute

revised:
  by: Mirek Kratochvil
  date: 2024-11-27
---

# Contributing to How-to Cards

How-to Cards are managed in [LCSB GitLab group
`lcsb/howto`](https://gitlab.lcsb.uni.lu/lcsb/howto), and developed using
`git` and Markdown.

To get a login in LCSB GitLab, you will need a [LUMS
account](/access/lums-passwords).

**If you are not familiar with `git` and want to edit a How-to Card, please
refer to the following guides:**

- [How-to Cards formatting with Markdown](/contribute/markdown) explains how
  the source code of the cards is written
- [Contributing via GitLab web interface](/contribute/gitlab) explains a
  straightforward way to submit your edits to the How-to Cards

**If you are familiar with `git`:** the contributions follow the usual `git`
workflow: You commit your changes into the target repository, open a merge
request, and How-to Cards maintainers will review and merge your request as
soon as possible. If you are familiar with `git`, the documentation present in
the GitLab group page should provide you with sufficient guidance.

We maintain the documentation of the best practices for contributing with
`git`:
- use of the GitLab [Web IDE](/contribute/web-ide) to make more complex changes
- [installation](/contribute/install-git) any of the [available `git`
clients](/contribute/git-clients)
- setting up a [GitLab fork with mirroring](/contribute/mirror-fork)
- setting up [SSH key authentication](/contribute/ssh-key-generation)
- [making changes using VSCode](/contribute/vscode)
- [contributing to other's unfinished merge requests](/contribute/supersede)

**If you were asked to review a merge request in How-to Cards repository:**
- [reviewing merge requests](/contribute/review)

All cards relevant to the contribution process are collected in the
[contribution category](/tag/contribute).
