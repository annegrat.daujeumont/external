---
mount: contribute/gitlab
name: "Contribute using Gitlab web interface"
tags:
  - contribute/basic
---

# Contribute using GitLab web interface

This card shows how to use GitLab to update a How-to Card.

Before you start, please make sure that you have the correct access rights to
edit the repositories in [group
`lcsb/howto`](https://gitlab.lcsb.uni.lu/lcsb/howto). If you want to update any
internal cards, you might even see an error message once you try to access the
repository in GitLab.

To get the access, you first need a [LUMS account](/access/lums-passwords) for
logging into LCSB GitLab. After that, contact R3 via <lcsb-r3@uni.lu> to
request access to the repositories in the `lcsb/howto` group.

## Starting the contribution

To start, find the How-to Card that you want to edit in the development
previews for the How-to Cards. You can access the development previews via the
following links:

- [externally visible version](https://lcsb.pages.uni.lu/howto/external)
- [internal-only version](https://lcsb.pages.uni.lu/howto/internal)

(Accessing the development previews requires a VPN or university network
connection.)

Once you find the target card, you can follow the development link (typically
found on the right side of the page) to open the given card's source code in
GitLab web interface. The link typically looks like this:

![howto editing link](img/link.png)

Once you get to GitLab, you should see a large blue "Edit" button with a
dropdown. Press it, and pick the "Edit single file" option:

![pressing the edit button](img/editbutton.png)

If you do not see the button, you will need to request the necessary
permissions (as noted above).

## Editing the card

You will find yourself with a large editor window that contains a plaintext
[markdown](/contribute/markdown)-formatted contents of the card. You can edit
the card as in any other text editor.

Once you are satisfied with your changes, you will need to *commit* them --
i.e., make a small commented contribution object that describes the precise
changes that you created.

To create the commit, simple fill in a short plaintext comment into the
**Commit message** field, and write a name of your **target branch** below
that. The name of the target branch is not important (as long as it does not
conflict with other branch names such as `master`, but you should generally
start it with your initials, and write a very short "identifier" of your
changes. For example, a good branch name for a commit that fixes a typo would
be e.g. `jd-fix-typo` (here we assumed that your name is **J**ohn **D**oe, thus
the prefix with initials is `jd-`).

You also want to directly start a merge request to incorporate your changes,
which is done by ticking the box below the form. In the end, everything should
look roughtly as follows:

![before committing](img/commit.png)

When you reach the desired state, press the **Commit changes** button.

**Note:** if you do not have write permissions for the repositories, GitLab may
offer you to create a new fork with the branch. In such case, proceed as GitLab
suggests -- you should arrive at the same state after the fork is created.

## Submitting a merge request

If everything went right, you will see a "New merge request" form. In most
cases you can simply click the **Create merge request** button below, and
that's it:

![merge request creation](img/mr.png)

You may additionally improve the MR for the reviewers and operators by
commenting upon what specifically you changed and why the changes matter, but
that is not required (the changes will be displayed together with your merge
request). You can also **Assign yourself** to the merge request, which
simplifies the management of multiple merge requests for the reviewers and
operators. If you already know who should check your changes, add them as
reviewers.

## What happens after submitting the merge request?

After clicking the **Create merge request** button, the request will be
processed and eventually merged. Reviewers may typically comment on your
changes in a [review process](/contribute/review); you will be notified about
these via e-mail.

In case of any questions, feel free to just open incomplete merge requests and
add comments and explanations -- operators and reviewers will help you to
finalize the required changes into a perfect state.

## Previewing the changes

All merge requests automatically generate a "preview" version of the How-to
cards installation. You can access it via the **View app** button on the top of
the merge request page:

![preview button location in gitlab](img/preview.png)

Note that you will need to wait until the "pipeline passes" (and the green
check mark appears above the button). If there are any problems, the preview is
not going to appear.

Sometimes, the preview button says **View latest app**, or does not appear
automatically -- this typically happens whenever a new preview is being
prepared but the old preview is still accessible. Wait until the pipeline
passes and refresh the page (using e.g. `F5`) to get access to the up-to-date
preview button.

## Troubleshooting & further reading

In case of any questions or problems, contact R3 at <lcsb-r3@uni.lu>.

If possible, use the GitLab commenting and issues interface to communicate
about the How-to Cards contents -- the communication is much more organized
than via e-mail.

If you would like to understand the described workflow better, please consult
our course material for [Git Trilogy Part 1: Contribute using
GitLab](https://courses.lcsb.uni.lu/2022-05-31_git-trilogy-part-1/stable/#/),
and [other cards in this category](/tag/contribute).
