---
mount: contribute/ssh-key-generation
name: "Key-based Authentication on GitLab"
tags:
  - contribute/git
redirects:
  - contribute:ssh-key-generation
  - /external/contribute/ssh-key-generation/
  - /cards/contribute:ssh-key-generation
  - /external/cards/contribute:ssh-key-generation
  - /external/external/contribute/ssh-key-generation/
---


# Key-based Authentication on GitLab

Key-based authentication allows GitLab users to identify their device once on their account and avoid having to log in via their credentials every time. GitLab uses SSH protocol to communicate with Git.

SSH uses a public and a private key:
- The public key must be registered in GitLab, where it "identifies" your device.
- The private key must stay secret and preferably you should only keep it on a single device. The private key is the only key that can be used to authenticate your identity against the public key uploaded to GitLab -- if it becomes public, anyone can impersonate your identity and thus use your GitLab account. **Make sure you do NOT upload the private key anywhere by accident, not even to GitLab.**

# Prerequisites

To use SSH, one of the following must be installed on your device:

- The OpenSSH client, which comes pre-installed on GNU/Linux, macOS, and [Windows 10 via Powershell or WSL](https://docs.gitlab.com/ee/user/ssh.html#use-ssh-on-microsoft-windows).
- SSH version 6.5 or later. Earlier versions used an MD5 signature, which is not secure.

To view the version of SSH installed on your system, run the following command in your terminal (on Linux or macOS) or Git Bash (on Windows):

```
ssh -V
```

# Key Generation

::: danger-box
[Please note that the steps below describe SSH key generation]{.box-header}

If you already have one, **your existing key may be overwritten.**
:::

1. Start a terminal

2. Type the following command at the command prompt and run it after replacing your firstname and lastname:
  ```
  ssh-keygen -t ed25519 -C firstname.lastname@uni.lu
  ```
3. Accept the suggested filename and directory

4. Press `Enter` to continue. Defining a password is not necessary and can be skipped.

5. To copy the contents of your public key file into the clipboard, run the following command:

    - For macOS:
    ```
    tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
    ```
    - For Windows:
    ```
    cat ~/.ssh/id_ed25519.pub | clip
    ```

# Saving the SSH Key on GitLab

Once the content of the public key is copied in the clipboard, you need to register it with your GitLab account.

To save your key to your account:

1. Log in to GitLab using your LUMS account

2. Navigate to the `Preferences` menu

    ![preferences](img/preferences.png)
3. User settings will be displayed on the left-hand side menu. Click on the `SSH Keys` option

    ![ssh-menu](img/ssh-menu.png)

4. On the `SSH Keys` page, click on `Add new key` button to add your public key

    ![new-key](img/new-key.png)

5. Paste your new key into the `Key` box, name the key and save it by clicking the `Add key` button

    ![key-details](img/key-details.png)

# Troubleshooting

Please refer to the [GitLab Docs](https://docs.gitlab.com/ee/user/ssh.html#troubleshooting) or [create a ticket](https://service.uni.lu/sp?id=sc_cat_item&table=sc_cat_item&sys_id=c1d98bacdb9cf810ca53454039961917) for further assistance regarding the key-based authentication.
