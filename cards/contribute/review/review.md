---
mount: contribute/review
name: "Reviewing in GitLab"
tags:
  - contribute/workflows
redirects:
  - contribute:review
  - /external/contribute/review/
  - /cards/contribute:review
  - /external/cards/contribute:review
  - /external/external/contribute/review/

revised:
  by: Mirek Kratochvil
  date: 2024-11-27
---


# Reviewing in GitLab

Typically, before any changes to How-to Cards are accepted, it is required that they go through an informal review. Reviewing helps with discovering and fixing accidental mistakes and inconsistencies, and is generally considered a good practice.

If you have been desginated as the reviewer of a merge request, you will receive a notification from GitLab. Typically this means that someone wants to know your opinion on the given changes.

## Starting the review

To start, simply follow the link to the merge request where your review is requested. There, click the **Changes** tab:

![img1.png](img/img1.png)

## Navigating the review interface

On the left side, there is a list of the files impacted by the changes in this merge request:

![img2.png](img/img2.png)

- [A red square]{.color-red} next to a file means that the file has been removed.
- [An orange one]{.color-orange} means that the file has been modified.
- [A green one]{.color-green} means that the file has been added.

When you choose a text file, you can see the individual changes in that file:

![img3.png](img/img3.png)

- [Lines in red]{.color-red} have been deleted
- [Lines in green]{.color-green} have been newly added
- Changed lines are rendered as if removed and replaced by new ones -- the [old version of the line is red]{.color-red}, and the [new version is green]{.color-green}

Lines with no changes are grayed out and possibly collapsed. To uncollapse the lines, click the arrows:

![img4.png](img/img4.png)

For a more readable view with e.g. pictures integrated, click on the 3 dots on the right side, and select **View files @ xxxxx**:

![img5.png](img/img5.png)

## Writing the review

To make a comment on a line, place your mouse on the line number and click on the bubble that appears on the left side:

![img6.png](img/img6.png)

After writing the comment, click either **Start a review** or **Add comment now**. Choosing "Start a review" allows you to compile several comments before submitting the complete review at once. The review is finished by clicking **Finish review**, which also sends notifications to the authors. Choosing "Add comment now" sends the notifications immediately for each such comment.

## Suggesting changes

Suggesting changes directly simplifies communication of the desired target state; authors may then commit they changes without having to re-type them into their computer.

To suggest changes, start adding a comment, then click the button on the left:

![img9.png](img/img9.png)

You may also suggest replacement of multiple lines -- simply choose the desired line span using the drop-down on the top.

## Submitting the review

Once all required comments are written down, remember to "submit" the review using the **Finish review** button:

![img8.png](img/img8.png)

You are also given a choice to formally **Approve** the merge request. Repository maintainers may use the approval as a simple way to see that you think the changes are good to go, and decide to start merging the changes if sufficient approvals are given by reviewers.
