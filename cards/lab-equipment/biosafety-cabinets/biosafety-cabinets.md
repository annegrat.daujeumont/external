---
mount: lab/biosafety-cabinets
name: "Biosafety Cabinets: good practices"
tags:
  - lab/equipment
  - lab/good-practice
redirects:
  - /lab/utilization-of-bsc
  - lab:biosafety-cabinets
  - /external/lab-equipment/biosafety-cabinets/
  - /cards/lab-equipment:utilization-of-bsc
  - /external/cards/lab-equipment:utilization-of-bsc
  - /external/external/lab-equipment/utilization-of-bsc/
  - /cards/lab:utilization-of-bsc
  - /external/cards/lab:utilization-of-bsc
  - /cards/lab-equipment:biosafety-cabinets
  - /external/cards/lab-equipment:biosafety-cabinets
  - /external/external/lab-equipment/biosafety-cabinets/
  - /cards/lab:biosafety-cabinets
  - /external/cards/lab:biosafety-cabinets
---

# Biosafety Cabinets: good practices

To ensure a good work quality in a sterile and safe environment, it is essential to know how to work in a BSC and what are the best practices.

Please check the following short videos from The World Health Organization (WHO).

1.  [Introduction](https://www.youtube.com/watch?v=KHCT9OJqxPo&t=1) to Biosafety Cabinets

2.  [Preparation Steps](https://www.youtube.com/watch?v=4DoHJS8JL4U) prior to working in a biosafety cabinet (BSC)

3.  [Best Practices](https://www.youtube.com/watch?v=18QEJUA9XBs) for safe biosafety cabinet (BSC) usage

4.  Biosafety cabinet (BSC) [Incident Management](https://www.youtube.com/watch?v=aS_TCZTCcsI)

If you have any issue with a BSC, please send a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b) to the Support Team.

If you are not sure a BSC is the right hood to use for your experiment, please check the [How To Card](/lab/hoods) related to the different types of hoods or contact the LCSB safety officer.

## Recommendations

-   The user must wear **safety glasses**, **gloves** and a BSL2 **lab coat** while using a BSC.

::: centered-block
![PPE](img/ppe.png){.w1-2}
:::

-   Be aware that **UV-lights act only on surfaces exposed to them**, the surfaces in the shadow are not disinfected and a chemical disinfection is still necessary after a UV decontamination. UV lights are damaging plastic material and gives a false feeling of disinfection while not being 100% efficient. Because of this, we do not recommend the usage of UV lights and advise to prefer the usage of disinfectant.

-   The BSC must be **decontaminated before and after each use**. The interior surfaces should be wiped with an appropriate disinfectant that would kill any microorganisms that could be found in the cabinet. 70% ethanol is not effective against yeast and fungi, you should **use Bacillol 30 foam or tissues**.

::: centered-block
![image3.jpeg](img/image3.jpeg){.w2-5}
![image4.jpeg](img/image4.jpeg){.w2-5}
:::

-   The BSC should be **empty** when you start working. If material is stored inside the BSC (for instance micropipettes), they must be disinfected when you switch on the BSC.

Corrosive chemicals such as bleach should be avoided, but if used, should be followed with a wipe down of sterile water or 70% ethanol. Wipe the work surface, the inside walls, below the work surface and the collecting pan. Do not use non-sterile paper towels.

![image5.png](img/image5.png){.align-center}

-   Allow the cabinet to run for at least **10 minutes** after switching on.

-   Your **hands** and all the **materials** that enter in the BSC must be **decontaminated with 70% ethanol or Bacillol 30 tissues**.

-   Put **Incidin PLUS** in **the aspiration pump** and disinfect the pump tubing with **Ethanol 70% or Bacillol 30**.

![image6.jpeg](img/image6.jpeg){.w1-4 .align-center}

-   Control the cells in the **microscope** before starting to work to **detect contamination** in your cell culture (change in the medium color, cloudiness of the medium, cross-contamination with other cell lines, ...).

-  Plan your work : set up workspace in a direction **from clean to dirty**.

![image7.png](img/image7.png){.align-center}

-   **Never cover the front or rear grids** with any material, even temporarily.

-   **Avoid movement of materials or excessive movement of hands and arms** through the front access opening during use. After entering, allow the cabinet to stabilize before resuming work.
- Work inside the BSC at a minimum distance of 15cm from the sash opening.

-   Take care **not pass** with your hands **over open bottles, lids, flasks or tip boxes**.

-   When you open a flask or a medium, you can place the **lid with the opening up or down** on the bench, but make sure you put it **outside your working area**, at the back of the BSC so you do not pass over it with your hands or arms.

-   **Close open bottles, flask, falcons, tips boxes immediately after use**.

-   **Remove your waste as soon as you have finish your work**.

-   When work is completed, clear the workspace and clean the space with Ethanol 70% or with Bacillol 30. Please refer to the [Chemical and Biological Waste Management](/lab/waste).

- **Switch OFf the BioSafety Cabinet**
Lower the glass and turn off the main Key-Switch.

![image8.jpeg](img/image8.jpeg){.w2-3 .align-center}

## UV lights

Some of the biosafety cabinets are equipped with UV lights. You may want to turn them on or to ask to install UV lights on your cabinet.
**Please note that we do not recommend the usage of UV lights in biosafety cabinet.**
The efficiency of UV lights in Biosafety cabinet is very limited. Everything in the shadow of a tips box, pipette holder,... will not receive any photon from the UV.
The surfaces below the worktop of the biosafety cabinet are also in the shadow.

Among the additional reasons why we do not recommend UV lights:

- UV lights efficiency decreases with the square of the distance, making it inefficient where it would be the more interesting to have it.
- UV lights efficiency decreases with the age of the light bulbs.
- UV lights affect plastics lifetime and may damage items left in the Biosafety cabinet
- Dust and dirt protect "bugs" from UV light.
**=> UV lights can not replace cleaning and disinfection.**

Would you experience contamination issue or have doubts about good practices in your laboratory setup, create a ticket to get in touch with our biosafety officer that can help you to review your procedures and look at the laboratory setup.
