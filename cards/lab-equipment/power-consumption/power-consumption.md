---
mount: lab/power-consumption
name: "Power consumption of the equipment"
tags:
  - lab/equipment
redirects:
  - lab:power-consumption
  - /external/lab-equipment/power-consumption/
  - /cards/lab-equipment:power-consumption
  - /external/cards/lab-equipment:power-consumption
  - /external/external/lab-equipment/power-consumption/
  - /cards/lab:power-consumption
  - /external/cards/lab:power-consumption
---


# Power consumption of the equipment

As part of the My Green Lab certification and with the aim of reducing our electricity consumption,
we have launched a campaign to raise awareness regarding the electricity consumption of laboratory equipment.
The result is a [list](asset/2022-09-06_powerConsumption.xlsx) showing the power consumption of the main devices on standby and/or in use.
These instruments were classified into 4 categories according to their use and in agreement with the users.
These categories are represented by labels of different colours,
affixed directly to the equipment and representing the way to manage the use of this equipment from a power point of view.

When a equipment can be switched off, it is mentioned on the label the amount of Watts saved every hour the instrument is off.

The labels are available at BT1 on the support team desk (5th floor, office 503) and at BT2 on the support team reception desk (1st floor).

If a piece of equipment is not on the list, a measurement can be requested from the [support team](https://service.uni.lu/sp?id=sc_cat_item&sys_id=4afd12541b7bb01035ba993d1d4bcbe7&sysparm_category=4dbd16d01b7bb01035ba993d1d4bcb84).

## Always on
This equipment must be permanently switched on, either for reasons of constant use (incubator, fridge, etc.),
or because the start-up process consumes a lot of energy, it is preferable from an energy point of view to leave
the appliance switched on all the time (ball water bath). This equipment can be switched off if it is not to be used for an extended period.
This should be discussed with the various users and with the Instrument Care Team in the case of equipment under monitoring (incubators, fridges, freezers).

![img1.png](img/img1.png){.w2-5 .align-center}

## Switch off when not in use
The device can be simply switched off when not in use and switched on when required.

![img3.png](img/img3.png){.w2-5 .align-center}

## On timer
The appliance is connected to a timer; the switch-on time is specified on the label.
Timers can be requested from the Support Team.

![img2.png](img/img2.png){.w2-5 .align-center}

## Switch off at the end of the day
At the end of the day, the appliance can be switched off. The time after which the appliance can be switched off is indicated on the label.

![img4.png](img/img4.png){.w2-5 .align-center}
