---
mount: lab/fridges
name: "Fridges: maintenance"
tags:
  - lab/equipment
redirects:
  - lab:fridges
  - /external/lab-equipment/fridges/
  - /cards/lab-equipment:fridges
  - /external/cards/lab-equipment:fridges
  - /external/external/lab-equipment/fridges/
  - /cards/lab:fridges
  - /external/cards/lab:fridges
---


# Fridges: maintenance

Routine maintenance of fridges and freezers prevents from issues and preserves integrity of samples.

In the event of a fridge or a freezer would get damaged or have a failure due to a lake of maintenance on the user side, the cost of the repair or of the replacement will be charged to the group in charge of the device.

The fridges and freezers are under constant temperature monitoring. In order to avoid alarms, the monitoring has to be disconnected before the cleaning of 4°C fridges and the defrosting of -20°C/-80°C freezers (see [How to deactivate the alarm in Sensor4Lab](#how-to-deactivate-the-alarm-in-sensor4lab)).

**Please inform the Instrument Care team before every kind of maintenance by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) 24h before starting your maintenance**.

Always start your maintenance in the morning.

When the maintenance is finished and the temperature is back to normal, reply on the open ticket to let them know you are done.

## How to deactivate the alarm in Sensor4Lab

All the freezers are monitored via a software called Sensor4Lab. To avoid any alarm due to the defrosting, the first step of the defrosting process is always to put the monitoring on hold. This step is needed for the cleaning of 4°C fridges and defrosting of -20°C/-80°C freezers.

**Only technicians have access to Sensor4lab**. Please ask a technician of your team to disable the sensor associated to the fridge you want to clean or defrost.
-	If you’re a technician, go on [Sensor4lab](https://sensor4lab.lcsb.uni.lu/account/logon) and login with your credentials received from the Instrument Care Team. If you don’t have any credentials yet, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=dc205fd7dbec38105c72ef3c0b96191d&sysparm_category=af924f17dbac38105c72ef3c0b96194b)
-	Please follow the process explained [here](https://dutycall.lcsb.uni.lu/cards/alarm-management/deactivate-alarm/)

Please note that there is a 30 minutes delay between the moment that you disable the sensor and the response of the system. This means that you should disable the monitoring 30 min before you start the maintenance.

## How to clean a fridge

### Bioline: BioPlus – BioMidi

This section applies to the fridges from GRAM, models BioPlus and BioMidi.

![img5.png](img/img5.png){.w1-2 .align-center}

-	**Once per quarter**, the door and gasket must be inspected and cleaned with a mild soap.

Make sure the door and the gasket are dry before closing back.
-	**Once a year**, the cabinet should be cleaned as follow:
    -	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Sensor4Lab sensor](#how-to-deactivate-the-alarm-in-sensor4lab)
    -	Switch off the fridge and pull out the main plug
    -	Transfer all content in another fridge and clean the inside of the fridge using a mild soap solution or 70% ethanol
    -	Make sure the inside of the fridge is dry
    -	Reconnect the main plug and switch the fridge back on
    -	Wait until the pre-set temperature is reached before transferring the content of the fridge back
    -	Enable the Rmoni sensor and inform the Instrument Care team via ticket that you are done and that everything is back to normal

### Liebherr LKEXv / Liebherr LKPv

This section applies to the LIEBHERR fridges (141 and 360 L and 600L) and to the fridge part of the combined fridge / freezers from LIEBHERR LC.

![img6a.png](img/img6a.png){.w1-2 .align-center}

-	The appliance defrosts automatically in Liebherr LKEXv fridges.

The defrost water drains into a tray located below the evaporator.

 **Once per quarter**, the defrost water tray should be emptied and cleaned.

![img7.png](img/img7.png){.w1-2 .align-center}

-	**Once a year**, the cabinet should be cleaned as follow:
    -	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-deactivate-the-alarm-in-sensor4lab)
    -   Switch off the fridge and pull out the main plug
    -	Transfer the content of the fridge to another fridge and clean the inside with lukewarm water and mild detergent. **Ensure that no cleaning water penetrates into the electrical components of ventilation grids**
    -	Dry the inside of the fridge
    -	Reconnect the main plug and switch the fridge back on
    -	Wait until the pre-set temperature is reached before transferring back the content of the fridge
    -	Enable the Rmoni sensor and inform the Instrument Care team via ticket that you are done and that everything is back to normal

![img6b.png](img/img6b.png){.w1-2 .align-center}

-	The freezers LIEBHERR LGPv (600 L) defrost automatically, and therefore, the user does not need to do it.
-	**Once a year**, the cabinet should be cleaned as follow:
    -	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-deactivate-the-alarm-in-sensor4lab)
    -   Switch off the freezer and pull out the main plug
    -	Transfer the content of the freezer to another freezer and clean the inside with lukewarm water and mild detergent. **Ensure that no cleaning water penetrates into the electrical components of ventilation grids**
    -	Dry the inside of the freezer
    -	Reconnect the main plug and switch the freezer back on
    -	Wait until the pre-set temperature is reached before transferring back the content of the freezer
    -	Enable the Rmoni sensor and inform the Instrument Care team via ticket that you are done and that everything is back to normal
