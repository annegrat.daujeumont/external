---
mount: lab/lightcycler
name: "LightCycler: leave the virtual instrument mode"
tags:
  - lab/equipment
redirects:
  - lab:lightcycler
  - /external/lab-equipment/lightcycler/
  - /cards/lab-equipment:lightcycler
  - /external/cards/lab-equipment:lightcycler
  - /external/external/lab-equipment/lightcycler/
  - /cards/lab:lightcycler
  - /external/cards/lab:lightcycler
---


# LightCycler: leave the virtual instrument mode

## Error message

1.	Here are a few of the error messages you can encounter when the instrument is in Virtual mode:

- When opening the software, you might see the message: ***“Please activate an instrument before setting up a new run”***

- When loading a run template, you might see: ***“Template’s filter combinations do not match. Template is for a different sized instrument block”*** (this will also appear if you try to load a program designed for 96 well plates on the 384-well plate block and vice versa)

![img2.png](img/img2.png)

2.	To properly identify the issue and make sure you are in Virtual mode, simply check the ***“Instrument”*** line of the software. If you are in virtual mode, it will be the first word of the instrument name. If you are not in virtual mode, the name might be ***“New Instrument”*** or ***“LC480”***

- Virtual:

![img3.png](img/img3.png)

- Normal mode:

    - ***“Standby (no MWP)”*** means that the instrument is on, but no plate has been loaded

    - ***“Not connected”*** means that the instrument is off.

![img4.png](img/img4.png)

![img5.png](img/img5.png)

## Fix the issue

In order to leave the virtual mode please follow the steps:

1.  On the right-side panel, go to ***“Parameters”***

![img6.png](img/img6.png)

2. Click on ***“Instruments”***

![img7.png](img/img7.png)

3. In virtual mode, the name of the instrument will start with ***“Virtual”*** and the IP address will only contain 0.

![img8.png](img/img8.png)

4. In the ***“Instruments”*** drop down menu, select the instrument called ***“New Instrument”*** or ***“LC 480”*** (depending on the databases). This instrument will have a valid IP address

5. Click on ***“Make Default”***

![img9.png](img/img9.png)

6. Click on ***“Close”***

![img10.png](img/img10.png)

**You should now be able to use the instrument.**
