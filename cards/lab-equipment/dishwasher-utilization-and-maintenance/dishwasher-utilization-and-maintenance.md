---
mount: lab/dishwasher-utilization-and-maintenance
name: "Dishwasher: utilization and maintenance"
tags:
  - lab/equipment
redirects:
  - lab:dishwasher-utilization-and-maintenance
  - /external/lab-equipment/dishwasher-utilization-and-maintenance/
  - /cards/lab-equipment:dishwasher-utilization-and-maintenance
  - /external/cards/lab-equipment:dishwasher-utilization-and-maintenance
  - /external/external/lab-equipment/dishwasher-utilization-and-maintenance/
  - /cards/lab:dishwasher-utilization-and-maintenance
  - /external/cards/lab:dishwasher-utilization-and-maintenance
---


# Dishwasher: utilization and maintenance

Utilization of the Miele dishwashers on the 5th floor of BT1 and on the 1st floor of BT2.

## Description

![dishwasher_img_1.jpg](img/dishwasher_img_1.jpg){.w2-3 .align-center}

1. Control panel
2. Serial interface
3. Water intake
4. Sensor access for validation
5. Salt container port
6. Filter combination
7. Service panel
8. Containers for DOS 2 and DOS 4 dispensing systems (optional)
9. DOS drawer
10. Containers for neutralize (red) and liquid detergent (blue)
11. Drying unit
12. Reset button

![dishwasher_img_2.jpg](img/dishwasher_img_2.jpg){.align-center}

1. On/Off button
2. Door release
3. Display
4. Optical interface
5. Start button
6. Clear button
7. Selection button: up
8. OK button
9. Selection button: down

## Utilization

### **Loading the dishwasher**

1. Ensure that no acid or solvent residues, especially hydrochloric acid or chlorides, get inside the machine
2. Empty all glassware before loading into the machine
3. Remove all agar residues
4. Remove all labels, sealing wax residues and handwritten text on the bottles
5. Small parts should be secured in basket inserts
6. Load your items on the tray
7. Close the door

### **Selecting a program and running the dishwasher**

1. Click on ![dishwasher_img_3.jpg](img/dishwasher_img_3.jpg) to switch on the instrument
2. Select the “Chemistry Bio” program using the up and down arrows

    ![dishwasher_img_4.jpg](img/dishwasher_img_4.jpg){.align-center}

3. Click on ![dishwasher_img_5.jpg](img/dishwasher_img_5.jpg)  to start the program

### **Cancel a program during run**

1. To cancel the program, click on ![dishwasher_img_6.jpg](img/dishwasher_img_6.jpg)
2. The program is then interrupted
3. At water temperatures below 40°C, the following message appears on the display: “Program cancel (OK) or Continue (Clear)?”
4. At water temperatures above 40°C, the following message appears on the display: “Cancel program (OK)”
5. Confirm the program cancellation with ![dishwasher_img_7.jpg](img/dishwasher_img_7.jpg)
6. The following message appears on the display: “Program cancelled. Water drainage”
7. After the water has drained away, the program list returns to the display

### **Unloading**

1. When the program is finished, the message “End of program” is displayed on the screen
2. Click on ![dishwasher_img_8.jpg](img/dishwasher_img_8.jpg) to open the door
3. Unload the tray and store the items in the cupboards

## Maintenance

### **Reactivation**
1. This section is available in the dishwasher room
2. If the display shows “Reactivation”, salt needs to be added to the dishwasher **directly when the message appears! Do not run a cycle when reactivation is needed!**
3. Material needed: salt and container

    ![dishwasher_img_9.jpg](img/dishwasher_img_9.jpg){.w2-5 .align-center}

4. To fill the container, unscrew and remove the filter insert from the container

    ![dishwasher_img_10.jpg](img/dishwasher_img_10.jpg){.w1-3 .align-center}

5. Completely fill the salt container with reactivation salt (Miele, 7785780;LIMS ID: 3278) and replace the filter insert. The reactivation will only be effective if the container is completely full.

    ![dishwasher_img_11.jpg](img/dishwasher_img_11.jpg){.w1-3 .align-center}

6. Remove any mobile racks from the dishwasher cabinet

    ![dishwasher_img_12.jpg](img/dishwasher_img_12.jpg){.w2-5 .align-center}

7. Unscrew the plastic cap located at the top right-hand of the cabinet. A small amount of residual water will be in the cap. Take care, as it might be hot from the previous program.

    ![dishwasher_img_13.jpg](img/dishwasher_img_13.jpg){.w2-5 .align-center}

8. Screw the salt container firmly onto the socket

    ![dishwasher_img_14.jpg](img/dishwasher_img_14.jpg){.w2-5 .align-center}

9. Close the door
10. Select and start the “Reactivation” program
11. The system will automatically perform the reactivation
12. When the cycle is finished, open the door
13. Carefully unscrew the salt container in order to allow any water pressure to subside. Do not use force, if the container cannot be removed manually, contact the lab maintenance team
14. The salt container must be emptied in the sink
15. Screw the softener lid back on
16. Insert the mobile unit
17. Wash and rinse the salt container and filter cap with clear water

**Notes**:

* BT1: the salt is stored in the lower cupboard next to the right dishwasher

* BT2: some boxes are available in the cupboard above the sink and the main stocks are in the -2 stock room

### **Routine checks**

#### The filters in the base of the wash cabinet

1.	The filters of the cabinets need to be checked and cleaned regularly
2.	To clean the coarse filter, press the two lugs together, remove and clean the coarse filter

    ![dishwasher_img_15.jpg](img/dishwasher_img_15.jpg){.w2-5 .align-center}

3.	To clean the flat and micro-fine filters,
4.	Remove the fine filter which sits inside the micro-fine filter

    ![dishwasher_img_16.jpg](img/dishwasher_img_16.jpg){.w2-5 .align-center}

5.	To unscrew the micro-fine filter, turn twice counter clockwise

    ![dishwasher_img_17.jpg](img/dishwasher_img_17.jpg){.w2-5 .align-center}

6.	Then, pull out the micro-fine filter together with the flat filter
7.	Clean the filter
8.	Replace the filters by performing the above steps in the reverse order
9.	Ensure that the filters sit flat in the base of the wash cabinet
