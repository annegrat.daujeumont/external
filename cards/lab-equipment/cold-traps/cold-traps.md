---
mount: lab/cold-traps
name: "Cold traps: maintenance"
tags:
  - lab/equipment
redirects:
  - lab:cold-traps
  - /external/lab-equipment/cold-traps/
  - /cards/lab-equipment:maintenance_of_cold_traps
  - /external/cards/lab-equipment:maintenance_of_cold_traps
  - /external/external/lab-equipment/maintenance_of_cold_traps/
  - /cards/lab:maintenance_of_cold_traps
  - /external/cards/lab:maintenance_of_cold_traps
  - /external/lab-equipment/maintenance_of_cold_traps/
  - /cards/lab-equipment:cold-traps
  - /external/cards/lab-equipment:cold-traps
  - /external/external/lab-equipment/cold-traps/
  - /cards/lab:cold-traps
  - /external/cards/lab:cold-traps
---


# Cold traps: maintenance

## Labconco Centrivap concentrator and cold trap

The procedure describes the steps for routine maintenance of the Labconco Centrivap concentrators and cold traps.
This maintenance is important to preserve the integrity of the glass bottles inside the cold trap.

**The Instrument care officer** is responsible for cleaning the condenser once a year, changing the acid filter when necessary and filling the cold trap maintenance record sheet.

**The Users** are responsible for the routine maintenance of the instrument and filling the “cold trap maintenance record sheet”.

## Description

To ensure proper functioning of the centrivap concentrators and associated cold trap, a routine maintenance is necessary:

**_Personal Protective Equipment_**:
- Lab coat
- Nitrile gloves

1.	This routine maintenance is performed by the users of the instrument.
2.	If a glass bottle is used inside the cold trap, add 99% ethanol to the stainless steel trap until the glass bottle is at least two-thirds immersed.
3.	At the end of each week, the cold trap should be switched off and the condensed solvent in the glass bottle should be discarded in the corresponding waste container.
4.	If there is any ice in the stainless steel trap (outside the glass bottle), let it melt, drain the ethanol and replace it with fresh ethanol.
To drain, use the faucet on the left side of the cold trap.

![cold-trap_img_1.png](img/cold-trap_img_1.png){.wx1-3 .align-center}

5.	Fill the “cold trap maintenance record sheet “ (Appendix 01)
6.	Restart the cold trap at the beginning of the following week

**Once a year, the refrigeration system condenser of the Cold trap should be cleaned.**

**_Personal Protective Equipment_**:
- Lab coat
- Nitrile gloves
- Safety goggles

1.	This step is performed by a maintenance officer
2.	Switch off the instrument
3.	Use a vacuum cleaner with brush attachment
4.	Clean the condenser (on the left side of the cold trap) to ensure proper airflow for peak performance

![cold-trap_img_2.png](img/cold-trap_img_2.png){.wx1-3 .align-center}

5.	Fill the “cold trap maintenance record sheet “ (Appendix 01)
When necessary, the acid cartridge (LIMS ID: 10608) should be exchanged

6. When the cartridge looks wet (see arrow on the following picture), it should be exchanged for a new one.

![cold-trap_img_3.png](img/cold-trap_img_3.png){.wx1-4 .align-center}

This is done by a maintenance officer

- Switch off the instrument
- Unscrew the black cap out of the transparent container
- Remove the cartridge and replace it with the new one
- Close back the container
- Place the old cartridge in the chemical hood for evaporation in a retention container
- 24 hours later, trash it in a blue bin with yellow lid (UN3291)
