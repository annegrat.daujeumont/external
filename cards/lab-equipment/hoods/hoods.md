---
mount: lab/hoods
name: "Hoods: Laminar Flow - Fume Hood - Biosafety Cabinet"
tags:
  - lab/equipment
redirects:
  - lab:hoods
  - /external/lab-equipment/hoods/
  - /cards/lab-equipment:hoods
  - /external/cards/lab-equipment:hoods
  - /external/external/lab-equipment/hoods/
  - /cards/lab:hoods
  - /external/cards/lab:hoods
---


# Hoods: Laminar Flow, Fume Hood, Biosafety Cabinet

### What are the differences and when to use them?

At LCSB, you can find different types of hoods. In order to work in a clean and safe environment, it is important to know what are the differences between the different hoods and when to use them. To help you to identify the different hoods, each of them has a sticker reminding the protection it offers.

For any issue regarding a hood, please send a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b) to the Support Team.

For any concern regarding safety or quality, please send a ticket [here](https://service.uni.lu/sp?id=sc_cat_item&sys_id=4343cc0ddbb070505c72ef3c0b961932&sysparm_category=cca7f2c1db683c905c72ef3c0b961940).

## Spot the difference

![](media/image1.png)

## Laminar Flow

A (vertical or horizontal) laminar flow is used when there is a need for
sample protection. It can be used only when the user does not have to be
protected from any biohazard or chemical risk, as a laminar flow does
not protect the user at all.

![](media/image2.png)

## Fume Hood

A fume hood is used with volatile chemical compounds. The air is drawn
out of the hood directly to the exhaust air. It can be used only when
the sample and the user do not have to be protected from any biohazard
risk. The fume hood is not a sterile environment.

![](media/image3.png)

## Biosafety cabinet (BSC)

A BSC is used when a sterile environment and/or protection of the user
against biohazard risks are needed. The biohazard protection of the user
is ensured by an air aspiration at the entrance of the BSC, creating an
air curtain minimizing the escape of aerosols. 70% of the air extracted
from the BSC is filtered by a HEPA filter and blown back in the BSC,
ensuring a sterile vertical flow. 30% of the extracted air is filtered
by another HEPA filter and blown to the laboratory. Therefore, BSC are
not protecting the users against chemicals risks.

![](media/image4.png)

## Biosafety cabinet (BSC) with thimble connection

At LCSB, we have some BSC with a thimble connection allowing to work in
a sterile environment with volatile chemical compounds. Thanks to the
thimble connection, the 30% of the air that is blown in the laboratory
in a normal BSC configuration is drawn in the exhaust air of the
building. This kind of hood is providing a sterile environment for the
sample protection and a protection of the user against biohazard and
volatile chemicals.

![](media/image5.png)
