---
mount: lab/pH-meter
name: "pH meter: utilization and maintenance"
order: "PH meter"
tags:
  - lab/equipment
redirects:
  - lab:pH-meter
  - /external/lab-equipment/pH-meter/
  - lab:utilization-of-pH-meter
  - /external/lab-equipment/utilization-of-pH-meter/
  - /cards/lab-equipment:utilization-of-pH-meter
  - /external/cards/lab-equipment:utilization-of-pH-meter
  - /external/external/lab-equipment/utilization-of-pH-meter/
  - /cards/lab:utilization-of-pH-meter
  - /external/cards/lab:utilization-of-pH-meter
  - /cards/lab-equipment:pH-meter
  - /external/cards/lab-equipment:pH-meter
  - /external/external/lab-equipment/pH-meter/
  - /cards/lab:pH-meter
  - /external/cards/lab:pH-meter

revised:
  by: Sandy Thill
  date: 2024-11-22
---


# pH meter: utilization and maintenance

This How-to Card describes the routine maintenance of pH meter “Seven Easy” and “Seven Compact” from Mettler-Toledo.

## Description

![description_pH-meters.jpg](img/description_pH-meters.jpg)

## Personal protective equipment

Always wear a lab coat, safety glasses and gloves when working with the pH meter.

![PPE.png](img/ppe.png){.w2-5 .align-center}

## Maintenance

1. All calibration solutions should be kept away from the light.
2. Any spillage has to be cleaned immediately. Use wipes to dry the spill, then clean with water.
3. The electrode has to be constantly immersed in a 3 M KCl solution.
    a. Do not allow it to dry.
    b. Change the solution at least monthly.
    c. Indicate the date of change in the log book.
    d. If the filling solution is encrusted outside the electrode, remove it by rinsing the electrode with deionized water.
4. Contact the quality officer if the logbook is full.
5. If there is an issue with the instrument, [create a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b).



## Calibration

If you are the first user of the day, a calibration should be performed (write the date and the number of points in the logbook).
You can also perform a calibration if you have doubts about the calibration status of the instrument.  
The pH meter allows for calibration of 1, 2 or 3 points.
The calibration standard solutions are of pH 4, 7 and 10.

1. Rinse the electrode with [MilliQ Type 1 water](/lab/water/#ultrapure-water-milli-q-water), wipe carefully the outside of the electrode with a lint free tissue.
2. Place the electrode in a calibration standard and press “CAL”.
3. The pH meter automatically endpoints when calibrating.
4. For 2 points calibration, repeat steps 1 to 3 for the second calibration standard.
5. For 3 points calibrations, repeat steps 1 to 3 with the third calibration standard.
6. Rinse the electrode with [MilliQ Type 1 water](/lab/water/#ultrapure-water-milli-q-water), wipe carefully the outside of the electrode with a lint free tissue.
7. After calibration, to return to sample measurement, press on:
    a. “Read” for the Seven Easy;
    b. “End”, then “Save” for the Seven Compact.
8. Add the calibration date in the equipment logbook.

### How to prepare the calibration reagents

1. The calibration cannot be performed using the reagents directly in their original containers, they need to be aliquoted.
2. Transfer a few millilitres of each of the three calibration solutions in a 15 mL falcon tube. Label them with the pH value of the solution and the date of aliquot.
3. Indicate the opening date on the original bottles and store them in the dark.
4. Keep the aliquoted solutions for a month, then replace them with fresh ones.
5. Indicate the date of exchange of the solutions in the log book.

## Measuring the pH

1. pH is temperature sensitive, therefore, make sure that your solution is at the temperature you will use it (for example: 37°C for cell culture medium).
2. Make sure the sample has been thoroughly mixed.
    - If you use magnetic stirrer, make sure you stop the stirrer before inserting the probe.
3. Rinse the electrode with deionized water, wipe carefully the outside of the electrode with a lint free tissue.
4. Place the electrode in the sample and press on “READ” to start the measurement.
5. During measurement, the decimal point of the display flashes.
6. Once the measurement is complete, the display freezes automatically.
7. Rinse the electrode with deionized water and place it back in its storing solution container (a 15 mL falcon containing 5 mL of 3 M KCl for example).
8. Adjust the pH of your solution, use a Pasteur glass pipet if you use strong acids or bases.
