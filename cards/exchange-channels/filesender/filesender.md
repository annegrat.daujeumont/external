---
mount: exchange-channels/filesender
name: "FileSender"
tags:
  - exchange-channels
  - data
revised:
  by: Jenny Tran
  date: 2024-10-09
---

# FileSender 
## Overview 
FileSender is a web-based transfer tool that allows you to easily and securely transfer small data with people. The tool is hosted by [The Restena Foundation](https://restena.lu/) and supports end-to-end encryption.  

## Transfer data with FileSender 
1. Go to [FileSender](https://fs.restena.lu/).
2. Select "University of Luxembourg".
![unilu-fs-instance.png](img/unilu-fs-instance.png)
3. Login with UNI-LU credentials when directed to the EduID authentication page. 
![login-uni-credentials.png](img/login-uni-credentials.png)
4. When logged in, you will be presented with the home page shown below. 
![filesender-home-page.png](img/filesender-home-page.png)
5. Use the *"drag & drop"* feature or the button *"Select files"* to upload data. 
![fs-upload.png](img/fs-upload.png){.align-center}
6. Enter the recipient's email. For multiple recipients please separate by a comma or semi-colon.
Please double-check that you have entered the correct email addresse(s), because whoever receives this email will have access to the data. 
![fs-recipient-email.png](img/fs-recipient-email.png){.w1-2 .align-center}
7. OPTIONAL: Write a subject and message to the recipient.
8. [**IMPORTANT**]{.highlight}: Tick the box "File Encryption". This is a requirement, when transferring confidential and/or sensitive human data. 
9. Enter a strong encryption password. You can use the password generator provided by FileSender.
![file-encryption.png](img/file-encryption.png){.w1-2 .align-center}
10. Copy password and share it securely via PrivateBin. Please follow the [How-to Card on sending password securely](/exchange-channels/sharingPasswords). FileSender does not store passwords, when you leave this page, you will not be able to see the password.
![fs-notifications.png](img/fs-notifications.png){.w1-2 .align-center}
11. OPTIONAL: To receive notifications for the data transfer, tick the relevant boxes as shown below.  
12. Press the send button. The recipient(s) will receive an email from FileSender containing a personalised link to the data. 

## Download data from FileSender
1. Open the download link in the invitation email sent from FileSender.  
![fs-email.png](img/fs-email.png){.align-center}
2. You will be directed to the web application of FileSender. Login with your UNI.LU credentials and press "Download" as shown below. <br>
![fs-download.png](img/fs-download.png){.align-center}
3. Enter the encryption password shared via PrivateBin.
![enter-password.png](img/enter-password.png){.align-center}
4. FileSender will automatically download the files locally on your computer and display "Download complete" when finished. 
5. Per default, the data provider will be notified  about successful download via email.

## Invite externals to transfer data with FileSender
FileSender is not exclusively for UNI-LU staff, but also externals can be invited to share their data with FileSender. An authenticated user (logged in with UNI-LU account) will have to send a "guest voucher" to the person, inviting them to the RESTENA server.

1. Log into FileSender and go to the "Guest"-tab. 
![fs-voucher.png](img/fs-voucher.png){.align-center}
2. Tick the box "Can only send to me".  
3. Enter the e-mail address of the external collaborator, whom you wish to receive data from via FileSender. 
4. OPTIONAL: Write a subject and message to the external collaborator.
5. Push "Send Voucher". 
6. The external collaborator will receive an e-mail to upload data to FileSender as shown below. 
![fs-email-voucher](img/fs-email-voucher.png){.align-center}
7. The guest can now upload files to FileSender and push the "Send" button when done. 
