---
mount: exchange-channels/sharingPasswords
name: "Sharing your passwords"
tags:
  - exchange-channels
redirects:
---

# Securely sending passwords to others

When sending passwords to collaborators, it is **NOT** advised to use e-mail or instant messaging. The preferred way is to generate **password links**, and then send these links to collaborators. Password links can have varying expiry periods; a link may expire once visited, or it be valid for a certain duration. Below are a list of freely available tools that can be used to generate password links. We recommend that you use  **PrivateBin hosted by the LCSB**.

|Tool|Notes|
|-----|------------------------|
| [PrivateBin @ LCSB](https://privatebin.lcsb.uni.lu) (Recommended)|Free service provided by the LCSB. LUMS account required. |
| [PrivateBin @ Uni-LU HPC](https://hpc.uni.lu/privatebin/) |Free service provided by the HPC team of Uni LU. No sign up required.|
| [Zerobin](https://zerobin.net/) | Free. No sign up required.|
| [Noteshred](https://www.noteshred.com) | Free. Sign up required.|

## PrivateBin hosted by the LCSB

### Sending a password
  1. Go to [PrivateBin @ LCSB](https://privatebin.lcsb.uni.lu/) and type the password you want to share into the Editor tab. You will be asked to enter your LUMS credentials when you click on "Send".

  ![privatebin-editor.png](img/privatebin-editor.png)

  2. Set expiry period. For the highest safety measures, sender can allow password link to be used only once so it expires upon first access. This feature can be set by checking *"Burn after reading"* checkbox.

  3. You can also enter a password to protect your password link - this should be different from the password you are sharing!
  4. Click *"Send"* in right-top corner. You should be redirected to a page containing the password and password link.

  ![privatebin-link.png](img/privatebin-link.png)

  5. Share the link with your collaborator via your favorite communication channel.

### Receiving a password
  1. You have been provided a link to website where your new password is stored. This link can be protected by yet another password you might be asked to enter.

![privatebin-password.png](img/privatebin-password.png)

2. If you see following:

![privatebin-expired.png](img/privatebin-expired.png)

  You have either entered the wrong/incomplete link - try copy-pasting the link into your browser instead of clicking on it - or your password link has already expired. In that case you will have to request a new link.

**Warning!**: If the password link was set with one access only (*Burn after reading*) and you are sure that you haven't accessed the link so far, you should notify the password sender because there is a chance that your communication was intercepted and someone accessed the link before you.

**Note**: If the page looks different from the pictures above, try using Google Chrome browser. If the issue persists, please take a screenshot of the page and send it to our [sysadmin](mailto:lcsb-sysadmins@uni.lu) team.