---
mount: exchange-channels/data-transfer
name: "Data transfer"
tags:
  - exchange-channels
  - data
redirects:
revised:
  by: Jenny Tran
  date: 2024-10-07
---

# Data Transfer
This How-to Card describes transferring data at LCSB and is split into two sections: 
- [Description of available services for data transfer at LCSB](#data-transfer-finder) 
- [Important factors to consider when transferring data](#what-to-consider-when-transferring-data) 

For questions and/or support on transferring your research data, please contact the LCSB data stewards via <lcsb-datastewards@uni.lu>.

## Data Transfer Tools
Use the guide below to compare different UL-LCSB transfer options for your specific use case. 

::: wide-block
[![data transfer finder](img/dtf.svg)](img/dtf.svg)
:::

### LCSB Large File Transfer (LFT)
LFT is ideal for transferring large (TBs) and/or sensitive datasets, e.g. whole genome data, transcriptomics data, clinical data, etc. To use LFT, one must install the IBM Aspera software locally and request a data transfer link by creating a dedicated ticket on [ServiceNow](https://service.uni.lu/sp?id=sc_cat_item&sys_id=4421fe5a1b3a2410ff1c8739cd4bcb4f&sysparm_category=e3a0fe5a1b3a2410ff1c8739cd4bcb4a), which will be handled by one of the LCSB data stewards. 

For more information on the usage of LFT, please visit the dedicated [How-to Card on LFT](/exchange-channels/lft).

### FileSender 
FileSender is a web-based application for fast, easy, and secure transfer of small files. The tool supports encryption, which must be manually enabled. FileSender is ideal for one-time transfer of sensitive data such as phenotype data. 

For more information on the usage of Filesender, please visit the dedicated [How-to Card on FileSender](/exchange-channels/filesender). 

::: info-box
[Note]{.box-header}
The maximum transfer size is browser dependent. You can upload files of any size up to 100 GB for browsers supporting  HTML5 FileAPI. For non-HTML5 browsers the upload limitation is 2 GB. 
:::

### LCSB ownCloud
ownCloud is mainly offered as a cloud storage solution managed by LCSB, but can in some cases be used for transferring small data < 4 GB to collaborators. To use ownCloud for data transfers, the sender must have a LUMS account to create a share link, while this is not needed for the recipient, whom can access the share link once granted. To ensure authorized access, the share link should be created with a strong password and time-restriction. The password should then be shared via secure means such as PrivateBin. 

**Other special use cases when using ownCloud:** 
- Transfer of sensitive data when LFT is **NOT** feasible: In this case, ownCloud can be used for transferring sensitive data by archiving the data at rest with a software supporting AES256 encryption. Please see more information on the following [How-To Card](/exchange-channels/owncloud-transfer).  
- Recurrent sharing of sensitive data: this use case describes data sharing of sensitive data, in which a cloud cryptor must be used together with ownCloud, e.g. [Cryptomator](/exchange-channels/cryptomator). 

### UL OneDrive 
As described in the previous section, OneDrive is mainly for data storage and data sharing, which makes OneDrive ONLY ideal when none of the above solutions are possible. To create a transfer, a public link must be created. For personal data the link must be created with a strong password and with time-restriction. 

Please refer to Microsoft's [documentation](https://support.microsoft.com/en-us/office/video-use-expiration-dates-and-passwords-d4a5a9d9-5cc8-416d-8bb9-90d6c17fd919) for further information on using expiration dates and passwords.

### LCSB PrivateBin 
LCSB PrivateBin is mainly for sharing passwords securely for data access. However, the tool offers the functionality of attaching a file when creating a password link. As PrivateBin is not designed for transferring data, it is only recommended when none of the above is possible. 

## What to consider when transferring data? 
This section describes important factors to consider when transferring data. 

### Legal framework covering data transfer
Before transferring data between LCSB and collaborators, a legal agreement must exists for covering the data transfer. These agreements comes in various forms:
- Data Transfer Agreement (DTA)
- Data Sharing Agreement (DSA)
- Material (and Data) Transfer Agreement (MDTA): e.g. for transferring cell lines
- Terms of Service (ToS)
- Non-Disclosure Agreement (NDA)
- Consortium Agreement (CA)

If no legal agreement exists for the intended data transfer, please initiate the legal process by contacting the the LCSB legal team via email <LCSB-Legal@uni.lu>.

::: danger-box
[Data transfer outside EU]{.box-header}
Transferring personal data outside of the EU-countries requires additional data protection safe guards, such as including Standard Contractual Clauses for the data transfer. In this case, please contact the the LCSB legal team via email <LCSB-Legal@uni.lu>. 
:::

### Checksums
Generating checksums both before and after data transfer is important to verify the data integrity, e.g. that data has not been unintendedly corrupted during transfer. Checksums should be sent together with the data and compared compared to the calculated checksums on the data received. Several algorithms exists for generating checksums such as SHA and MD5.

For further information, please visit the following card on [how to generate checksums](/integrity/checksum).

### Record activity in DAISY
DAISY is a registry for recording research projects and data processing activities at LCSB. When transferring data, it is thus important to record data transfer activities in DAISY, and especially when transferring personal data due to the General Data Protection Regulation (GDPR). 

The transfer should be recorded in the specific dataset logbook with information on data sender, data recipient, and storage location for data received to LCSB.

The legal agreements covering the data transfer mentioned in the [above section](#legal-framework-covering-data-transfer) should also be uploaded to DAISY.

Please visit the [user guide](https://elixir.pages.uni.lu/daisy-doc/), for further information on usage of DAISY.



