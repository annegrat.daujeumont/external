---
mount: exchange-channels/calendar
name: "Sharing calendar in Microsoft Exchange"
tags:
  - exchange-channels
redirects:
  - exchange-channels:calendar
  - /external/exchange-channels/calendar/
  - /cards/exchange-channels:calendar
  - /external/cards/exchange-channels:calendar
  - /external/external/exchange-channels/calendar/
---

# Sharing calendar in Microsoft Exchange

If you don't want to be overwhelmed by doodle polls and multiple rescheduling of meetings, you can share your calendar with your collaborators so they can see when you are busy.

## Sharing calendar via Outlook
  1. Log-in into Outlook ([https://outlook.office.com/mail/](https://outlook.office.com/mail/)).

  2. Navigate to calendar (top-left corner).

  ![an_owa_mail.png](img/an_owa_mail.png)

  3. Click on **Share**.

  ![an_owa_calendar.png](img/an_owa_calendar.png)

  4. Add people you want to share your calendar with.

  ![an_owa_sharing-detail.png](img/an_owa_sharing-detail.png)

  5. Choose a role for each of your collaborators. This defines how much information related to your events will be visible to others.

  6. Send the invitation to your calendar.

## Sharing calendar via Outlook Client
In order to share calendar using Outlook Client:
  1. Navigate to calendar tab.

  ![an_outlook_mail.png](img/an_outlook_mail.png)

  2. Click on **Share Calendar**.

  ![an_outlook_calendar.png](img/an_outlook_calendar.png)

  3. Add your collaborators you want to share your calendar with as recipients.

  4. Choose amount of shared information about your events.

  ![an_outlook_sharing-detail.png](img/an_outlook_sharing-detail.png)

  5. Send the invitation to your calendar.
