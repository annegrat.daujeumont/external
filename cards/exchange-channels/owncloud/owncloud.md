---
mount: exchange-channels/owncloud
name: "LCSB OwnCloud"
tags:
  - exchange-channels/owncloud
  - data
redirects:
  - exchange-channels:owncloud
  - /external/exchange-channels/owncloud/
  - /cards/exchange-channels:owncloud
  - /external/cards/exchange-channels:owncloud
  - /external/external/exchange-channels/owncloud/
---

# LCSB OwnCloud

**LCSB ownCloud**  is a private cloud storage service for the use of LCSB staff and collaborators. It is suitable for exchanging small-sized files (up to 1-2 gigabyte). All communication with the LCSB ownCloud server is SSL encrypted. Additionally, the OwnCloud storage is backed up at regular intervals (as of January 2025, daily snapshots are kept for 30 days, monthly for 12 months and yearly for 10 years).

## Obtaining a LUMS account

A LUMS (LCSB User Management System) account is needed in order to use LCSB ownCloud. LUMS accounts for LCSB staff are normally created within the first few days of starting work at the LCSB. In addition to the staff, researchers at partner institutes may be given LUMS accounts. To create a LUMS account, please open a ticket on [ServiceNow](https://service.uni.lu/) under the category LCSB IT -> BioCore -> Account Management Services -> [LUMS - New account](https://service.uni.lu/sp?id=sc_cat_item&sys_id=c536257ddb336010ca53454039961936&sysparm_category=6b76697ddb336010ca534540399619f8).

## Using LCSB ownCloud

Similar to other cloud storage systems, ownCloud is accessible both via a browser and also via a client application. On the web, LCSB's ownCloud is at [https://owncloud.lcsb.uni.lu/](https://owncloud.lcsb.uni.lu/).

![LCSB ownCloud web login](img/owcld_1.png)

User documentation on ownCloud tools and portal can be found [here](https://doc.owncloud.com/).

When sharing research data, one should observe the following guidance using Owncloud:

* Limit folder shares to only the personnel that needs to access data.

* When sharing via links, always set a **password** and an **expiration date** for the link.

  ![](img/owcld_2.png)

* When sharing folders via links, the link passwords should **NOT** be sent plain in email. Instead, password links should be used. If you need to generate link passwords can use the [Private bin](https://privatebin.lcsb.uni.lu) service hosted by the LCSB.

 **IMPORTANT NOTE**: Whenever the LCSB shares an ownCloud folder with you (collaborator), the share password will be reachable via link, which will expire. Therefore you should make a record of your password once you view it.

## Installing Owncloud Desktop Client

**Important:** ownCloud is fetching information from LUMS, therefore changes like a password reset (see How-to Cards [Changing password for LUMS accounts](//?lums-passwords)) should be performed **only** via LUMS!

1. Please try following link to download the Owncloud Desktop Client depending on your operating system (this is an example for Windows, similar steps for other OS) - [Owncloud Client](https://owncloud.com/desktop-app/).

2. Start the ownCloud client installation by clicking twice on the downloaded file.

3. Owncloud Setup Wizard will pop-up, like the image below.

    ![setup-wizard-01.png](img/setup-wizard-01.png)

4. Click **Next**.

5. Clic **Next** when Custom Setup window appears.

    ![custom-setup-01.png](img/custom-setup-01.png)

6. Click **Install** on Ready to Install window.

7. Click **Yes** when asked if you want to allow this app to install software on your PC.

    ![user-account-control.png](img/user-account-control.png)

**Note:** If you do not have admin rights, please ask SIU to provide you the admin rights on the laptop or do the installation for you.

8. Click **Finish** when the ownCloud setup wizard completes.

    ![setup-wizard-complete.png](img/setup-wizard-complete.png)

9. Please enter **owncloud.lcsb.uni.lu** when the following window appears.

    ![server-address.png](img/server-address.png)

10. Enter User Credentials.

    * Username - firstname.lastname
    * Password - LUMS password

    ![user-credentials.png](img/user-credentials.png)

11. It may ask you to trust the **certificate**, please do so by clicking **OK**.

12. Once you are done with above steps, you should see something as below.

    ![local-folder-options.png](img/local-folder-options.png)

13. You can choose if you want the ownCloud folder in a different location by clicking on **Local folder** bar as shown in above image.

14. From above image, you can also choose what to sync, as you may not need all the folders downloaded on your latpop, which you do not need.

15. You can select on unselect folders and subfolders to your preference. The desktop client enables you to sync files from your desktop to the ownCloud server. You can create folders in your home directory and keep the contents of those folders synced with your ownCloud server. You can also select the files/folders that you want to remove from your local file system.

    **Important:** the file/folder will be still stored on the server!

16. Then select **connect** and you are done.

17. Once the synchronization is finished, you should see something as below.

    ![synchronization.png](img/synchronization.png)

18. From the activity you can see what was **synced**, **not synced** and **server activity**

    ![activity.png](img/activity.png)

19. On windows, the ownCloud bar is on the right hand corner under hidden icon **^**.

20. For quick access to ownCloud folder, please go to **Local Disk (C:)** > **Users** > **yourname** and then right click on the Owncloud folder and select **Pin to Quick access**.

    ![quick-access.png](img/quick-access.png)
