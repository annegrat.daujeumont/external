---
mount: lab/lab-coats
name: "Lab coats"
tags:
  - lab/hsa
  - lab/ppe
redirects:
  - lab:lab-coats
  - /external/lab-hsa/lab-coats/
  - /cards/lab-hsa:lab-coats
  - /external/cards/lab-hsa:lab-coats
  - /external/external/lab-hsa/lab-coats/
  - /cards/lab:lab-coats
  - /external/cards/lab:lab-coats
revised:
  by: Annegrait Daujeumont
  date: 2025-02-24
---


# Lab coats

Which lab coat should I wear? Where do I get a new lab coat and where do I dispose a dirty one?
Each person entering the laboratories must be trained on proper lab coat use and waste disposal protocols to maintain the integrity of the laboratory environment.

## Wear the right lab coat in the right area

The general rule is that **BSL1 lab coats** should be worn in all the **BSL1 laboratories** and **BSL2 lab coats** should be worn in all the **BSL2 laboratories**. Each researcher should have a dedicated BSL2 lab coat for each BSL2 lab he/she is working in.

### Specific cases

To ensure compliance with **safety rules** while allowing the **biotech support team**, **external people**, and **cleaning team** to carry out their duties effectively, clear guidelines for the appropriate use of lab coats in BSL2 laboratories have been established. 

#### Support Biotech Team Members (Checking Only):

Support biotech team members who are only checking parameters or conducting non-invasive tasks must wear a BSL2 lab coat.
This lab coat should be stored in a dedicated cupboard, with one cupboard assigned for the support biotech team per floor.
  **Movement between BSL2 labs**: When performing checks, team members may move between different BSL2 labs and cross the Open Lab without needing to remove their BSL2 lab coat.

#### Support Biotech Team Members (Intervention, Experiment or Repair)

If the support biotech team members are performing interventions, experiments or repairs, they must wear a dedicated BSL2 lab coat specific to the room they are working in.
This ensures that contamination risks are minimized within each laboratory environment.

#### External People (Checking Only)

External people, including the infrastructure team, performing only checks must wear a disposable lab coat.
These disposable lab coats can be used across multiple rooms, but should be disposed of after the tasks are completed.

#### External People (Intervention or Repairs)

When external people are required to perform interventions or repairs, they must wear either a dedicated BSL2 lab coat or a disposable lab coat dedicated to a single room.
This is crucial to prevent cross-contamination between rooms and ensure safe handling of equipment and infrastructure.

#### Cleaning Team

The cleaning team is not allowed to touch anything other than the chairs and must wear a BSL1 lab coat.
The BSL1 lab coat can be used in all BSL2 rooms. Since changing lab coats when entering each room would introduce a greater risk of contamination, the cleaning team may wear the same lab coat across multiple rooms.
General Notes:

- Lab coats should never be taken outside of the designated laboratory areas to prevent contamination.
- Any lab coat, whether BSL1, BSL2, or disposable, must be properly stored or disposed of at the end of the task.



![1.png](img/1.png)

### Lab coat mandatory

![2.jpg](img/2.jpg)

- All BSL1 and BSL2 laboratories
- Storage Rooms : Chemical Storage/ cryostorage/LCSB stock
- For all users, visitors, technicians.

### Lab coat forbidden

![3.png](img/3.png)

- Offices and open offices
- Kitchen, Coffee Points (red flooring)
- Toilets

### Lab coat allowed

![4.png](img/4.png)

- Lift
- Staircases
- Corridor to the stairs/lift
- Corridor to the support team zone BT2 1st floor (trolleys/ fridges) __BUT not the support team offices__
- **ONLY FOR BSL1 LAB COAT**

## Where can I find a new clean lab coat?

### BT1

Clean lab coats are stored in the cupboard of the room BT1-E05-502, on the 5th floor. They are sorted by Biosafety Level (BSL1 and BSL2) and sizes. For the larger sizes, you can find them with extra lengths on the sleeves (+15cm).

### BT2

Clean lab coats are stored in the cupboards of the MUF room, on the 1st floor, close to the Support Biotech Team offices. They are sorted by Biosafety Level (BSL1 and BSL2) and sizes. For the larger sizes, you can find them with extra lengths on the sleeves (+15cm).

## What should I do with my dirty lab coat?

On  a regular basis you should bring your lab coat in the tissue bags close to the vending machine on the ground floor in BT1 or BT2.

Remove the pins and empty the pockets.

Someone from the Support Biotech Team will take care to send the lab coats for cleaning.

![7.jpg](img/7.jpg){.w3-4}

## Pins
To identify your lab coat as yours, do not forget to pin your pin on it.

Create a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=050f18e08788855007eeed309bbb35b6&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) to request your pins.

## Help needed?

Any question regarding lab coats, safety glasses or other PPE? [Please send a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940).
