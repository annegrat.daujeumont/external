---
mount: lab/shipment
name: "Shipment of biological or chemical samples with carrier"
tags:
  - lab/hsa
  - lab/transport
redirects:
  - lab:shipment
  - /external/lab-hsa/shipment/
  - /cards/lab-hsa:shipment
  - /external/cards/lab-hsa:shipment
  - /external/external/lab-hsa/shipment/
  - /cards/lab:shipment
  - /external/cards/lab:shipment
---


# Shipment of biological or chemical samples with carrier

The Support Biotech Team at LCSB organises the biological or chemical samples shipments with carriers. They have been trained and certified by IATA (International Air Transportation Association) to be allowed to prepare the shipping documents for dangerous goods.

For shipments of documents or non-lab related goods, please contact the administrative assistants.

## 1) Prepare the package

A **triple packaging** is necessary for any transport outside the building, by air or by road.

- **First packaging** refers to your initial container, eg the Eppendorf, Falcon tube or culture plate. The tubes or plates should be closed with parafilm to avoid any leakage.

- **Secondary packaging** is ensured when you pack the first packaging in a plastic bag. Plastic bag of different sizes are available in the Support Biotech Team office. When preparing your package please make sure to add enough absorbing material in the secondary packaging.

- **Third packaging** is the box used for transport. It can be a styrofoam or cardboard box sealed with tape. You can find tape in the Support Biotech Team office.

*Tips*
- Dedicated bags can be used for the transport of biological material.
- Dedicated box can be used for the transport of chemicals.
- Do not overlap the labels.
- Do not change the size of the labels provided to you.
- The tape should not hide the labels.

![triple-packaging.mp4](https://webdav.lcsb.uni.lu/public/R3lab/howto-files/triple-packaging.mp4)

## 2) Choose the carrier

### Fedex or DHL:
- Transit time within European Union: 1 or 2 days
- Transit time outside European Union: up to 5 days

If the samples must be kept frozen, please fill the package with dry ice (at least 5kg for EU shipment and 15kg for non EU shipment). We recommend you to place an order of dry ice in Quarks by Wednesday at the latest. The deliveries of dry ice occur every Monday.

Shipping days:
- European Union: Monday, Tuesday or Wednesday for Room Temperature shipment
- European Union: Monday or Tuesday for shipment with dry ice
- Outside European Union: Monday for a delivery by Friday. In case the package must be delivered on Monday we can schedule a shipment on Thursday or Friday (the package will be in transit all weekend long)

*Please note these are recommendations and delays at the customs might occur.*

**How to organise a shipment with Fedex or DHL?**

Please send a ticket few days in advance via [Service Portal](https://service.uni.lu/sp?id=sc_cat_item&sys_id=9cf7f205db683c905c72ef3c0b9619cd&sysparm_category=ca67f2c1db683c905c72ef3c0b9619a8) and make sure to fill all information. If an information is missing, the shipment might be postponed.

If you are sending a GMO to the USA, it is mandatory to enclose the [emergency response procedure](emergency-response-procedure.pdf) to your package.

The Support Biotech Team will review the information provided in the ticket and will make sure all the safety requirements are fullfiled (shipping temperature and nature of the goods). They will provide the shipping documents and labels to be enclosed to the package and give you all the instructions to follow in the ticket.

Depending on the current situation we will go either for Fedex or DHL carrier. If you have specific request, please mention it in the ticket.

### World Courier:
If you would like to ship precious samples we recommend World Courier as carrier. It is an expensive service but they offer the guarantee to maintain your samples at the right temperature. (e.g.: if the samples must be kept frozen they can come with a styrofoam box full of dry ice and refill it until the delivery. A secondary packaging of your samples is sufficient.)

**How to organise a shipment with World Courier?**
1.  Complete the [Commercial Invoice](commercial-invoice-template.docx):
    -   Date of the shipment
    -   Shipper address and contact
    -   Consignee address and contact
    -   Commodity description **(it is important to give a full description of your samples)**
    -   HS Code
        - 3002590000 is for cell cultures, whether or not modified
        - 3821000000 is for cell culture media
        - 0106900090 is for research animals (mice, zebrafish)

        If the commodities are not listed above please check the HS code via this [link](https://ec.europa.eu/taxation_customs/dds2/taric/taric_consultation.jsp?Lang=en&Expand=true&SimDate=20230327) or contact the Lab Safety Officer.

     -   Quantities and value
     -   Send the commercial invoice to the Lab Safety Officer for signature

2. Send an email to request a quote to <QuoteBelgium@worldcourier.com> and <bruops@worldcourier.be>. Please also add <aa-lcsb@uni.lu> in cc so they can create a Purchase Order. Please attach the commercial invoice signed by the Lab Safety Officer and specify if you need them to provide dry ice and a transport box and if you need a temperature logger.

3.  After receiving the quotation by World Courier, the Support Biotech Team takes care of the Purchase Order and send it to World Courier.

4.  When all the necessary documents have been sent to World Courier they can schedule the shipment and give you the instructions (print and enclose documents to the package).

*Please bear in mind that the process can take few weeks depending on the type of documents they need for customs clearance.*

# Reception of biological or chemical samples
In case a collaborator needs to ship samples to LCSB and we have to cover the shipping costs, the Support Biotech Team can organise the shipment to LCSB.

### Fedex or DHL:
Please send a ticket few days in advance via [Service Portal](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6a3e4701dbe83c905c72ef3c0b961903&sysparm_category=ca67f2c1db683c905c72ef3c0b9619a8).

*Please note we can also provide our Fedex or DHL account number to the collaborators in case they would like to organise the shipment. They must add the OTP in reference.*

### World Courier:
Please follow the procedure mentioned above
