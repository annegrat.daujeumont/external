---
mount: lab/pass
name: "Personal alert safety system (PASS)"
tags:
  - lab/hsa
  - lab/emergency
  - lab/ppe
redirects:
  - lab/personal-alert-safety-system
  - lab:personal-alert-safety-system
  - /external/lab-hsa/personal-alert-safety-system/
  - /cards/lab-hsa:personal-alert-safety-system
  - /external/cards/lab-hsa:personal-alert-safety-system
  - /external/external/lab-hsa/personal-alert-safety-system/
  - /cards/lab:personal-alert-safety-system
  - /external/cards/lab:personal-alert-safety-system
---


# Personal alert safety system (PASS)

Utilization of the PASS “Loner M6” (BlackLineSafety) available in BT1 and BT2.
PASS is a device used by a person working alone in a potentially dangerous environment.
The device allows calling for rescue in case of problem.
The call can be triggered voluntarily or automatically, in the event of a fall for example.

The lone worker should pick up and register the utilization of the PASS in the logbook.
After use, they should ensure the device is correctly plugged to charge the battery.

## Overview

1.	The phones detect if the user is:

    a.	falling
    b.	immobile for more than 5 minutes

2.	Therefore, the PASS automatically sets off a pre-alarm, which warns the user.
If this pre-alarm is not reset, the PASS calls the emergency number 5555.
3.	In case of emergency, the PASS can also be used to call manually 5555.
4.	It is recommended when:

    a.	working in isolated rooms,
    b.	performing dangerous activities
    c.	working alone during the evenings or weekends

5.	It is forbidden to perform dangerous experiments during the night and weekends (including work with Piranha solution, highly corrosive or explosive compounds).

## Handling the device

### Description of the System

![pass_img-1.jpg](img/pass_img-1.jpg)

### Quick user guide

::: wide-block
![pass_img-2.jpg](img/pass_img-2.jpg)
:::

### How to obtain the PASS

1.	The PASS are stored by the guard stations in both BT1 and BT2.

::: centered-block
![pass_img-3.jpg](img/pass_img-3.jpg){.w2-5}
![pass_img-4.jpg](img/pass_img-4.jpg){.w2-5}

![pass_img-5.jpg](img/pass_img-5.jpg){.w2-5}
:::

2.	To obtain the PASS, place your BT2 badge in front of the lock until the light is green.
3.	Fill in the logbook.

### Switch on

1.	Press on the power button.
2.	A single alarm will sound, the alarm light array will flash, and the device will vibrate once.
The power button and SureSafe indicator light (green) will continue to flash while establishing a network connection.

3.	Wait for the device to establish a network connection, which can take up to one minute.
4.	Once connected, the SureSafe indicator light will remain continuously on.

### Fastening the emergency phone to the clothing

5.	The PASS can be fastened to the clothes using the clip located in the back of the unit.
6.	Hold the PASS in a vertical position while fastening it.

### Switch off

7.	 Press and hold the power button for approximately 3 seconds (until the device stops beeping).
8.	The alarm will sound three long beeps while the power button is pressed the device vibrates for three long pulses.

The alarm, vibration motor and power button and SureSafe indicator light will turn off after the shutdown is complete.

### Return the PASS

9.	When you are done with the PASS, place it back on the charger, a red light will flash to indicate that the battery is charging.
10.	Fill the logbook and close the box by placing your BT2 badge in front of the lock.

## Types of alerts

### Emergency alert

1.	Pull the emergency latch. The device will instantly send an emergency message to the 5555.
2.	The device will trigger the “high alarm” pattern to notify others nearby of the event.
3.	To silence the alert, return the emergency latch to the closed position.
4.	Press and hold the acknowledge button until the alarm pattern is silenced (approximately 3 seconds).
The device will return to regular operation.
5. ![pass_img-6.jpg](img/pass_img-6.jpg) silencing the alert will not cancel it: the 5555 will still intervene.

### Fall impact detected alert

The device is constantly monitoring your motion or falls. If the device detects a fall impact, it will trigger the “pending alarm” mode.

If you do not require assistance, you have 30 seconds to cancel the alert by pressing on the acknowledge button.

1.	Cancelling a pending fall detected alert.

    a.	The device will start a pending alarm.
    b.	Press the acknowledge button until the alarm pattern is silenced   (approximately 1 second).
    The device will return to regular operation.

2.	Silencing a fall detected alert.

    a. If you have not cancelled the alert within 30 seconds, the fall detected alert is communicated to the 5555.
    b.	The device will go to “high alarm”.
    c.	Press and hold the acknowledgement button until the alarm pattern is silenced (approximately 3 seconds).
    The device will return to regular operation.

    d. ![pass_img-6.jpg](img/pass_img-6.jpg) Silencing an alarm will not cancel the call, 5555 will still intervene.

### No-motion alert

The device is constantly monitoring your motion to detect a possible person down incident.
If the device has not detected motion within 5 minutes, it will prompt you to acknowledge your safety, entering the alarm mode and triggering the low alarm pattern.
If you do not require assistance, you have 30 seconds to cancel the alert by pressing on the acknowledgement button.

1.	Cancelling a pending no-motion alert.

    a.	The device will start a “pending alarm”.
    b.	Press the acknowledge button until the alarm pattern is silenced (approximately 1 second).
    The device will return to regular operation.

2.	Silencing a no-motion alert

    a.	If you have not cancelled the alert within 30 seconds, the no-motion alert is communicated to 5555.
    b.	The device will trigger the “high alarm” pattern and will continue until the alert is cancelled or the battery runs out.
    c.	Press and hold the acknowledge button until the alarm pattern is silenced (approximately 3 seconds).
    The device will return to regular operation.
    d. ![pass_img-6.jpg](img/pass_img-6.jpg) Silencing an alarm will not cancel the call, 5555 will still intervene.

### Voice call alert

1.	When a phone call has been initiated by 5555, the device will trigger the “high alarm” pattern accompanied by a blue flashing of the LiveResponse light.
2.	To silence the alarm, press and hold the acknowledge button until the alarm is silenced (approximately 3 seconds).
3.	The LiveResponse confirmation light will continue to flash, signifying that an incoming call has been initiated.
4.	Once the call has been connected, the device will generate a long beep signifying the beginning of the two-way  voice calling.
5.	In a noisy environment, it may be necessary to remove and hold the device near your face, as you would do with a two-way radio.
6.	Once the call is complete, the caller will end the call.
The device will trigger a long beep signifying the end of the call.
7.	The device user cannot end the call.
8.	The LiveResponse confirmation light on the device will go off once the call has ended.
9.	NOTE: during the call, the volume of the speaker can be switched to speaker and back to lower volume by pressing the acknowledge button once.

### Low battery notification

1.	If the battery level on the device drops below 20% capacity, the device will flash the charging light and beep once every 5 minutes.
Once the device battery is depleted, it will automatically shut down.
2.	This is considered an unsafe log off.
If the device is configured to require a periodic check-in, a low battery shutdown will not stop the timer and a missed check-in alter will be generated.

## Types of alarms

|Type|Beeper|Vibration|Visual indicator|
|------|------|---------|--------------|
|**Pending alarm**|Slow alarm pattern|Single repeating vibration|Alarm light array slow flash (red)|
|**High alarm**| Fast alarm pattern| Double repeating vibration|Alarm light array fast flash (red)|
|**Low battery**|Single beep once every 5 minutes|No vibration|Charge light, single flash once every 5 minutes|
