---
mount: lab/waste
name: "Waste Management: Chemical and Biological waste"
tags:
  - lab/hsa
  - lab/waste
redirects:
  - lab:waste
  - /external/lab-hsa/waste/
  - /cards/lab-hsa:waste
  - /external/cards/lab-hsa:waste
  - /external/external/lab-hsa/waste/
  - /cards/lab:waste
  - /external/cards/lab:waste

revised:
  by: Sandy Thill
  date: 2024-10-09
---


# Waste Management: Chemical and Biological waste

To ensure a clean and safe lab environment, it is essential to know how to discard your waste in a proper way.
Please follow the [General Lab training](https://learn-develop.uni.lu/Catalog/training/1034528) (part 7) and refer to the [Chemical and Biological Waste Management concept of the University](https://uniluxembourg.sharepoint.com/sites/hsso/Style%20Library/Forms/AllItems.aspx?id=%2Fsites%2Fhsso%2FStyle%20Library%2FImages%2Fhsso%2FLists%2FList%20Activits%2FAllItems%2FHSE%2D05%2D014%2DFIQ%2DALL%2DChemical%20and%20biological%20waste%20management%20%2D%20Rev%202%2E0%2Epdf&parent=%2Fsites%2Fhsso%2FStyle%20Library%2FImages%2Fhsso%2FLists%2FList%20Activits%2FAllItems).

For any question regarding safety and waste management, please send a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) to the LCSB lab safety team.


## Chemical waste

### How to label your waste

There are waste labels per category that have to be placed on your chemical waste.

You have to fill the label with your research group, your laboratory (the place where the waste was produced) and the date.

![label.png](img/label.png){.w1-2 .align-center}

### How to identify the correct waste category

If you do not know where to trash your chemical waste, you have 3 methods to find out:

1. Go to your [team's waste concept](https://uniluxembourg.sharepoint.com/sites/hsso/Lists/List%20Activits/DispForm.aspx?ID=19&Source=https%3A%2F%2Funiluxembourg.sharepoint.com%2Fsites%2Fhsso%2FSitePages%2FSST.aspx&ContentTypeID=0x0100D2EEE88241032E468E3192A9C475BB58&RootFolder=)

First open the concept belonging to your building.

::: wide-block
![concept.png](img/concept.png)
:::

  a. Go to the excel sheet of your group *(in this example DVB)*
  b. Find the name of the product you want to trash *(in this example nitric acid)*
  c. Get the waste code corresponding to your product *(in this example CW12)*
  d. Trash your product in the correct barrel in the chemical waste room

::: wide-block
![excel.png](img/waste_excel.png)
:::

2. Check the [decision tree](https://uniluxembourg.sharepoint.com/sites/hsso/Style%20Library/Forms/AllItems.aspx?id=%2Fsites%2Fhsso%2FStyle%20Library%2FImages%2Fhsso%2FLists%2FList%20Activits%2FAllItems%2FHSE%2D05%2D014%2DFIQ%2DALL%2DChemical%20and%20biological%20waste%20management%20%2D%20Rev%202%2E0%2Epdf&parent=%2Fsites%2Fhsso%2FStyle%20Library%2FImages%2Fhsso%2FLists%2FList%20Activits%2FAllItems) of the university waste concept

::: wide-block
![1.png](img/1.png)
:::

For more information about how to correctly read this decision tree, have a look in the [e-learning](https://learn-develop.uni.lu/Catalog/training/1034528) (part 7). 
There you can find detailed explanations about the decision tree.

3. If you can still not find the correct waste group, open a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) and place it on the *Special Waste Shelf* in the chemical waste room with a CW/E label. 
On this label, precise which product(s), which concentration(s), and again your research group, your laboratory (the place where the waste was produced) and the date.


### How to know if you can mix waste together

Check the section **Incompatible materials** in the material safety data sheet – Section 10.

::: wide-block
![section10.png](img/section10.png){.align-center}
:::

## Infectious waste

In general, we differenciate between normal infectious waste and infectious waste contaminated by hazardous chemicals and/or toxins.

### Contaminated with chemicals and/or toxins
Infectious waste contaminated by hazardous chemicals and/or toxins should be discarded in the blue bins with yellow lids:

![blue_bin_yellow_lid.png](img/blue_bin_yellow_lid.png){.w1-4 .align-center}

When full, these bins have to be transferred in the chemical waste room on dedicated areas.

Note: If there are huge amounts of chemicals (for example acids) in your biological waste, it becomes *Special Waste* and you need to open a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) on service now before discarding it.

### Non contaminated with chemicals and/or toxins

#### Solids
For solids, place the waste in the nalgene bins that are being autoclaved. Put 2 plastic bags in one nalgene bin.
Afterwards, the autoclaved waste has to be transposed in a domestic waste bag in order to be evacuated to the domestic waste by the building cleaning team.

![solids.png](img/solids.png){.w1-4 .align-center}

#### Liquids
For liquids either autoclave it or use *Incidin Plus* to decontaminated the solutions for at least 30 minutes before it can be put in the sink.

![liquids.png](img/liquids.png){.w2-5 .align-center}

### Biological waste from cell culture

For the correct disposal of waste coming from cell culturing (pipettes in contact with biologicals, used medium, etc) please refer to the corresponding [How-to Card](/lab/bio-waste-management/).



## Sharps

For the correct handling and disposal of sharps (broken glassware, small glass vials, needles, tweezers, scalpels, etc) please refer to the corresponding [How-to Card](/lab/sharps/).

Sharptainers containing material contaminated with infectious material or infectious material mixed with chemicals have to be discarded in blue bins with yellow lids.

::: centered-block
![sharptainer.jpg](img/sharptainer.jpg){.w1-3}
![sharptainer_in_blue_bin.jpg](img/sharptainer_in_blue_bin.jpg){.w1-3}
:::

Sharptainers containing material contaminated only with chemicals should have a CW17 label and be discarded in the blue CW17 barrel in the chemical waste room.

![sharptainer_cw17.jpg](img/sharptainer_cw17.png){.align-center}

::: info-box
[Additional information]{.box-header}
When full, barrels and blue bins with yellow lids must be transferred from the lab in the chemical waste room on dedicated areas.
:::