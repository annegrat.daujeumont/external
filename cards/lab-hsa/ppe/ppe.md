---
mount: lab/ppe
name: "Personal Protective Equipment (PPE)"
tags:
  - lab/hsa
  - lab/ppe
redirects:
  - lab:ppe
  - /external/lab-hsa/ppe/
  - /cards/lab-hsa:ppe
  - /external/cards/lab-hsa:ppe
  - /external/external/lab-hsa/ppe/
  - /cards/lab:ppe
  - /external/cards/lab:ppe
---

# Personal Protective Equipment (PPE)

To protect every person present in the laboratories from any contamination or exposure to hazardous materials, chemical or biological, each person entering the laboratories has to wear the appropriate Personal Protective Equipment (PPE).

All LCSB employees, students and visitors are responsible for wearing the appropriate PPE that will vary depending on the hazards present in the room, but must always include:

- Your **lab coat** to protect yourself as it represents an additional layer of protection for skin in case of accidental contact and splashes of hazardous substances (chemical or biological). It is also a removable barrier and it protects body’s area not completely covered by clothing. The lab coat also prevents contamination inside and outside the lab.

  More information about lab coats can be found in the dedicated [How-to card](/lab/lab-coats). 

- Your **safety glasses** to protect your eyes from accidental droplets and splashes of hazardous substances (chemical or biological).

  More information about safety glasses can be found in the dedicated [How-to Card](/lab/safety-glasses).

- Appropriate **disposable gloves** when handling biological materials or chemicals

  ![Gloves1.png](img/Gloves1.png){.align-center}

  More information about gloves can be found in the dedicated [How-to card](/lab/gloves). 

- **Specific PPEs** depending on the performed activity (face shield, thermal gloves, thermal apron, ...).

Other protective measures have to be taken to reduce any contamination risk such as:

  - closed‐toe shoes
  - clothing that covers the legs
  - tie back long hair
  - avoid wearing jewelry that could come into contact with biological materials/chemicals or damage the gloves

## PPE Requirements

### Classical laboratories activities

For regular biomolecular, chemical, cell culture or microbiology work in BSL 1 or BSL 2 laboratories, the mandatory PPEs are:

- **Lab coat**

  The Lab coat must be tied up to top with sleeves fastened at the wrists, protecting the arms and torso.

- **Disposable powder free nitrile gloves**

- **Safety glasses**

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
:::

### Work with toxic chemicals

When handling toxic agents ex: DNA labelling reagents.

- **Lab coat**

  With sleeves fastened at the wrists, protecting the arms and torso.

- **Disposable powder free long sleeves nitrile gloves**

  Wear the glove above your lab coat.

Note: Disposable gloves must be discarded after each use or when they become contaminated.

- **Safety glasses**

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
:::

### Work with very strong chemicals

Dealing with strong chemicals such as strong acids, bases, ... requires specific gloves providing specific protection, on top of the regular PPEs.
Those gloves are not disposable gloves.

- **Chemical Gloves**: ANSELL: ChemTek 38-520

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img26.png](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
![img10.jpg](img/img10.jpg){.h1-4}
![img11.jpg](img/img11.jpg){.h1-4}
:::

### Autoclave unloading and other hot thermal risks

On top of the mandatory PPEs for BSL1 work, wear the **Ansell EDGE 16-500 gloves** located next to the autoclaves to protect you against hot thermal hazard. 
Those gloves are not disposable gloves.

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
![autoclaveglove.png](img/autoclave_glove.jpg){.h1-4}
:::

### Automated cryostorage

On top of the mandatory PPEs for BSL1 work, it is mandatory to wear:

- **Cryo-gloves**

  Those gloves are not disposable gloves.

- **Safety glasses**

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img2.jpg](img/img2.jpg){.h1-4}
![img24.png](img/img24.jpg){.h1-5}
:::

### Distribution of Liquid Nitrogen in a Dewar

On top of the mandatory PPEs for BSL1 work, it is mandatory to wear:

- **Cryo-gloves**

  Those gloves are not disposable gloves.

- **Face shield**

- **Cryo-apron**

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img2.jpg](img/img2.jpg){.h1-4}
![img27.jpg](img/img27.jpg){.h1-4}
![img28.jpg](img/img28.jpg){.h1-4}
:::

### Usage of Liquid Nitrogen in open Dewars

Some experiments require to snap freeze the samples in Liquid Nitrogen or to keep samples frozen in Liquid Nitrogen during the manipulation. Having Liquid Nitrogen in an open Dewar on the bench top requires to wear on top of the regular PPEs:

- **Cryo-gloves**

  Those gloves are not disposable gloves.

- **Face shield**

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img15.jpg](img/img26.jpg){.h1-4}
![img2.jpg](img/img2.jpg){.h1-4}
![img27.jpg](img/img27.jpg){.h1-4}
:::

### Other cold thermal risks

When handling samples in the -80°C freezers, in the -150°C freezer, in the cryopod or in dry-ice, it is recommended to wear, in addition to the regular mandatory PPEs cotton white gloves to be worn under the regular nitrile gloves. 
It will enable an improved dexterity compared to the regular cryo-gloves.  
Those cotton gloves are not disposable and should be kept in your lab coat.

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img6.jpg](img/img6.jpg){.h1-4}
![img26.png](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
:::

### Spill management

Spill management requires extra protection that you can find in the spill kit. 
For more information on spill management, please refer to the How-to card [Spill in a Laboratory](https://howto.lcsb.uni.lu/?lab:spill).
The PEE to wear depend on size of the spill.

#### Smalls spills (chemical and biological)

- Wear **chemical and cut resistant gloves** (ANSELL: AlphaTec 58-735) above **nitrile gloves**

To protect the skin against two types of risks: the chemicals and the glass/plasticware debris. 
Those gloves are disposable.

- **FFP2 mask with charcoal coating**

Ensure protection against particles and limited protection against chemical vapors due to the charcoal coating. 
A valve helps the breathing.

- **Lab coat** (BSL1 or BSL2 depending on the location of the spill)

- **Safety glasses**

- **Overshoes**

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img15.jpg](img/img26.jpg){.h1-4}
![img12.jpg](img/img12.jpg){.hx1-4}
![img13.jpg](img/img13.jpg){.h1-4}

![img19.jpg](img/img19.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
![img29.jpg](img/img29.jpg){.h1-4}
:::

#### Large spills (chemical or biological)

For large spills (>4L), where the risk of contaminating your clothes is much higher, wear the same PPE as for small spills, except that you replace your lab coat with a yellow Tyvek over-coat from the spill kit.

![img30.jpg](img/img30.jpg){.w1-3 .align-center}

### Work with non-tested biological samples

Collective protective equipment is always the preferred option to be protected against airborne biological hazards. 
Therefore, biological hazards should be manipulated under biosafety cabinets.  
However, if after discussion with the safety officer and/or after a risk assessment made in collaboration with the safety officer, it shows that it is impossible to work under a biosafety cabinet, then it is mandatory to wear, on top of the regular PPEs:

- **FFP2 mask with charcoal coating**

  Ensure protection against particles and limited protection against chemical vapors due to the charcoal coating. A valve helps the breathing.

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
![img19.jpg](img/img19.jpg){.h1-4}
:::

### Work with non-toxic smelling samples

To avoid unpleasant smells in the laboratories, it is recommended to work with non-toxic samples under a chemical hood or an aspiration arm.  
However, if after discussion with the safety officer and/or after a risk assessment made in collaboration with the safety officer, it shows that it is impossible to work under a chemical hood or an aspiration arm, then it is recommended to wear, on top of the regular PPEs:

- **FFP2 mask with charcoal coating**

  Ensure protection against particles and limited protection against chemical vapors due to the charcoal coating. A valve helps the breathing.

::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img24.jpg](img/img24.jpg){.h1-5}
![img19.jpg](img/img19.jpg){.h1-4}
:::

### Work with UV's light

**PROTECT EYES AND SKIN FROM EXPOSURE TO UV LIGHT**

- **Lab coat**

  The body needs to be fully covered with long trousers, closed shoes and lab coat. 
  The lab coat must be tied up to top with sleeves fastened at the wrists, protecting the arms and torso. 
  Lab workers must be particularly vigilant to prevent gaps in protective clothing that commonly occur around the neck and wrist areas.

- **Disposable powder free nitrile gloves**

  Hands must be protected with long purple nitrile gloves to ensure protection of the wrist.

- **Face Shield**

  As soon as the UV light is switched on, an anti-UV face shield is mandatory (if the protective glass of the equipment cannot be closed completely).
  Not all eye protections available in the lab will fit for this purpose!  
  Attention needs to be paid to the specification of the PPE; this should be done in accordance with the safety officer.


::: centered-block
![img23.jpg](img/img23.jpg){.h1-4}
![img25.jpg](img/img25.jpg){.h1-4}
![img26.jpg](img/img26.jpg){.h1-4}
![img31.png](img/img31.png){.h1-4}
:::
